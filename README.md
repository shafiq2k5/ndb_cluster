# NDB_CLUSTER

https://github.com/mysql/mysql-docker/blob/mysql-cluster/7.5/cnf/mysql-cluster.cnf


To implement a MySQL Cluster, we have to install three types of nodes. Each node type will be installed on it's own server. The components are:

##### 1. Management Node - NDB_MGMD/MGM

   The Cluster management server is used to manage the other node of the cluster. We can create and configure new nodes, restart, delete, or backup nodes on the cluster from the management node.

##### 2. Data Nodes - NDBD/NDB
    
   This is the layer where the process of synchronizing and data replication between nodes happens.

##### 3. SQL Nodes - MySQLD/API
   
   The interface servers that are used by the applications to connect to the database cluster.
# Prerequisites

The OS is CentOS 7 - 64bit.

5 CentOS servers or virtual machines. I will use the hostnames and IP addresses as shown below:

###### Management Node (MGM)

    db1 = 192.168.1.120
###### Data Nodes (NDB)
    
    db2 = 192.168.1.121
    db3 = 192.168.1.122

###### SQL Nodes (API)

    db4 = 192.168.1.123
    db5 = 192.168.1.124

# LINKS  
    https://www.howtoforge.com/tutorial/how-to-install-and-configure-mysql-cluster-on-centos-7/
 
 
 
# Conclusion
 
 MySQL Cluster is a technology that provides High Availability and Redundancy for MySQL databases. It uses NDB or NDBCLUSTER as the storage engine and provides shared-nothing clustering and auto-sharding for MySQL databases.  To implement the cluster, we need 3 components: Management Node(MGM), Data Nodes (NDB) and SQL Nodes (API). Each of node must have its own memory and disk. It is not recommended to use network storage such as NFS. To install MySQL Cluster on a CentOS 7 minimal system, we have to remove the mariadb-libs package, mariadb-libs conflict with MySQL-Cluster-server and you have to install the perl-Data-Dumper package, it's needed by MySQL-Cluster-server. A MySQL Cluster is easy to install and configure on multiple CentOS servers.

