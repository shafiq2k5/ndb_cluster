#!/usr/bin/env bash
#Step 2 - Setup the MySQL Cluster Data Nodes

#A. Login as root user and download the MySQL Cluster software
cd ~
wget http://dev.mysql.com/get/Downloads/MySQL-Cluster-7.4/MySQL-Cluster-gpl-7.4.10-1.el7.x86_64.rpm-bundle.tar
tar -xvf MySQL-Cluster-gpl-7.4.10-1.el7.x86_64.rpm-bundle.tar
ls -lah *.rpm

#B. Install and Remove Packages
yum -y install perl-Data-Dumper
#yum -y remove mariadb-libs

#D. Configure Data Node
cp my.cnf /etc/

#C. Install MySQL Cluster
cd ~
rpm -Uvh MySQL-Cluster-client-gpl-7.4.10-1.el7.x86_64.rpm
rpm -Uvh MySQL-Cluster-server-gpl-7.4.10-1.el7.x86_64.rpm
rpm -Uvh MySQL-Cluster-shared-gpl-7.4.10-1.el7.x86_64.rpm




mkdir -p /var/lib/mysql-cluster
ndbd

#E. Redo step 2.A - 2.D on db3 server.