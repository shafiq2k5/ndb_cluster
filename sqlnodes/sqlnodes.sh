#!/usr/bin/env bash
# Step 3 - Setup SQL Node

#A. Log in and Download MySQL Cluster
cd ~
wget http://dev.mysql.com/get/Downloads/MySQL-Cluster-7.4/MySQL-Cluster-gpl-7.4.10-1.el7.x86_64.rpm-bundle.tar
tar -xvf MySQL-Cluster-gpl-7.4.10-1.el7.x86_64.rpm-bundle.tar

#B. Install and Remove Packages
yum -y install perl-Data-Dumper
#yum -y remove mariadb-libs
#D. Configure the SQL Node

cp my.cnf /etc/
#C. Install MySQL Cluster
cd ~
rpm -Uvh MySQL-Cluster-client-gpl-7.4.10-1.el7.x86_64.rpm
rpm -Uvh MySQL-Cluster-server-gpl-7.4.10-1.el7.x86_64.rpm
rpm -Uvh MySQL-Cluster-shared-gpl-7.4.10-1.el7.x86_64.rpm

service mysql start

#E. Redo step 3.A - 3.D on db5 server.

