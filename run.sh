#!/usr/bin/env bash
docker network create cluster --subnet=192.168.0.0/16
#MANAGEMENT (MGM)
docker run -d --net=cluster --name=management --ip=192.168.0.2  mysql/mysql-cluster ndb_mgmd

#DATA NODE (NDB)
docker run -d --net=cluster --name=ndb1 --ip=192.168.0.3  mysql/mysql-cluster ndbd
docker run -d --net=cluster --name=ndb2 --ip=192.168.0.4  mysql/mysql-cluster ndbd

#MYSQL NODE (API)
docker run -d --net=cluster --name=mysql_api --ip=192.168.0.10  mysql/mysql-cluster mysqld
