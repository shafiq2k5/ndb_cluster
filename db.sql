-- MySQL dump 10.16  Distrib 10.2.11-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: shuni_live_standard
-- ------------------------------------------------------
-- Server version	10.2.11-MariaDB-10.2.11+maria~jessie

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account_emailaddress`
--

DROP TABLE IF EXISTS `account_emailaddress`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_emailaddress` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(254) NOT NULL,
  `verified` tinyint(1) NOT NULL,
  `primary` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `account_emailaddress_user_id_2c513194_fk_auth_user_id` (`user_id`),
  CONSTRAINT `account_emailaddress_user_id_2c513194_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_emailaddress`
--

LOCK TABLES `account_emailaddress` WRITE;
/*!40000 ALTER TABLE `account_emailaddress` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_emailaddress` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_emailconfirmation`
--

DROP TABLE IF EXISTS `account_emailconfirmation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_emailconfirmation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `sent` datetime(6) DEFAULT NULL,
  `key` varchar(64) NOT NULL,
  `email_address_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`),
  KEY `account_emailconfirm_email_address_id_5b7f8c58_fk_account_e` (`email_address_id`),
  CONSTRAINT `account_emailconfirm_email_address_id_5b7f8c58_fk_account_e` FOREIGN KEY (`email_address_id`) REFERENCES `account_emailaddress` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_emailconfirmation`
--

LOCK TABLES `account_emailconfirmation` WRITE;
/*!40000 ALTER TABLE `account_emailconfirmation` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_emailconfirmation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_agency`
--

DROP TABLE IF EXISTS `apps_agency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_agency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maxclient` int(11) NOT NULL,
  `useclient` int(11) NOT NULL,
  `creditlimit` int(11) NOT NULL,
  `usecredit` int(11) NOT NULL,
  `monthlyrefill` int(11) NOT NULL,
  `creditdate` datetime(6) NOT NULL,
  `user_id` int(11) NOT NULL,
  `agency_name` varchar(45) NOT NULL,
  `contact_no` varchar(45) NOT NULL,
  `profile` varchar(100),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_agency`
--

LOCK TABLES `apps_agency` WRITE;
/*!40000 ALTER TABLE `apps_agency` DISABLE KEYS */;
INSERT INTO `apps_agency` VALUES (9,10,3,1000,151,1,'2018-05-02 07:52:12.357138',28,'DataShall Analytics Ltd','01988602086','152569768231513928_1699950853376461_3662889118100094976_n.jpg'),(10,7,0,15,0,1,'2018-04-23 07:11:13.975377',32,'arefin crop','01772744358','pic_folder/None/no-img.jpg'),(11,7,0,15,0,1,'2018-04-23 07:13:27.719131',33,'arefin crop','01772744358','pic_folder/None/no-img.jpg'),(12,7,0,15,0,1,'2018-04-23 07:47:55.864097',35,'farsi corp','01772744358','pic_folder/None/no-img.jpg'),(17,7,3,320,296,1,'2018-05-09 00:48:42.146501',42,'farsi org','01772744358','152594847620151218_100803000_iOS.jpg'),(18,4,0,8,0,1,'2018-05-02 11:26:07.084637',45,'test Agency','01715519132','pic_folder/None/no-img.jpg'),(19,10,4,100,74,1,'2018-05-10 00:54:55.429585',48,'shuni_brand','01111111','1525935200alex.png'),(20,6,0,100,0,1,'2018-05-02 09:17:21.095078',49,'corp','00000221001','pic_folder/None/no-img.jpg'),(21,5,0,1000,0,1,'2018-05-03 07:00:07.585521',53,'Datashall','989898989898','pic_folder/None/no-img.jpg'),(22,10,0,100,0,1,'2018-05-03 14:22:02.504500',54,'Farsi Agency','234234','pic_folder/None/no-img.jpg'),(23,10,0,100,0,1,'2018-05-03 14:28:16.361020',55,'Farsi Agency','234234','pic_folder/None/no-img.jpg'),(24,10,0,100,8,1,'2018-05-07 02:47:25.713892',56,'Farsi Agency','234234','152568291720151213_184241000_iOS.jpg');
/*!40000 ALTER TABLE `apps_agency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_agencyclient`
--

DROP TABLE IF EXISTS `apps_agencyclient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_agencyclient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creditlimit` int(11) NOT NULL,
  `usecredit` int(11) NOT NULL,
  `monthlyrefill` int(11) NOT NULL,
  `contact_no` varchar(45) NOT NULL,
  `user_id` int(11) NOT NULL,
  `creditdate` datetime NOT NULL,
  `profile` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `apps_agencyclient_user_id_4188858c_fk_auth_user_id` (`user_id`),
  CONSTRAINT `apps_agencyclient_user_id_4188858c_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_agencyclient`
--

LOCK TABLES `apps_agencyclient` WRITE;
/*!40000 ALTER TABLE `apps_agencyclient` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_agencyclient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_agencyuserlist`
--

DROP TABLE IF EXISTS `apps_agencyuserlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_agencyuserlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agency_admin` tinyint(1) NOT NULL,
  `agency_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `creditdate` datetime(6) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `apps_agencyuserlist_agency_id_f228478b_fk_apps_agency_id` (`agency_id`),
  KEY `apps_agencyuserlist_user_id_519c0c9a_fk_auth_user_id` (`user_id`),
  CONSTRAINT `apps_agencyuserlist_agency_id_f228478b_fk_apps_agency_id` FOREIGN KEY (`agency_id`) REFERENCES `apps_agency` (`id`),
  CONSTRAINT `apps_agencyuserlist_user_id_519c0c9a_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_agencyuserlist`
--

LOCK TABLES `apps_agencyuserlist` WRITE;
/*!40000 ALTER TABLE `apps_agencyuserlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_agencyuserlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_airprofile`
--

DROP TABLE IF EXISTS `apps_airprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_airprofile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `socialid` varchar(45) NOT NULL,
  `telcoid` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `totoal_comment` int(11) NOT NULL,
  `pur_pattern` int(11) NOT NULL,
  `spam` int(11) NOT NULL,
  `score` decimal(5,2) NOT NULL,
  `cumSum` decimal(5,2) NOT NULL,
  `sat_level` int(11) NOT NULL,
  `last_interection` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_apps_airprofile_socialid` (`socialid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_airprofile`
--

LOCK TABLES `apps_airprofile` WRITE;
/*!40000 ALTER TABLE `apps_airprofile` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_airprofile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_blprofile`
--

DROP TABLE IF EXISTS `apps_blprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_blprofile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `socialid` varchar(45) NOT NULL,
  `telcoid` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `totoal_comment` int(11) NOT NULL,
  `pur_pattern` int(11) NOT NULL,
  `spam` int(11) NOT NULL,
  `score` decimal(5,2) NOT NULL,
  `cumSum` decimal(5,2) NOT NULL,
  `sat_level` varchar(45) NOT NULL,
  `last_interection` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_apps_blprofile_socialid` (`socialid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_blprofile`
--

LOCK TABLES `apps_blprofile` WRITE;
/*!40000 ALTER TABLE `apps_blprofile` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_blprofile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_blprofile2`
--

DROP TABLE IF EXISTS `apps_blprofile2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_blprofile2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `socialid` varchar(45) NOT NULL,
  `telcoid` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `totoal_comment` int(11) NOT NULL,
  `pur_pattern` int(11) NOT NULL,
  `spam` int(11) NOT NULL,
  `score` decimal(5,2) NOT NULL,
  `cumSum` decimal(5,2) NOT NULL,
  `sat_level` varchar(45) NOT NULL,
  `last_interection` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_apps_blprofile_socialid` (`socialid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_blprofile2`
--

LOCK TABLES `apps_blprofile2` WRITE;
/*!40000 ALTER TABLE `apps_blprofile2` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_blprofile2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_campaignanalysis`
--

DROP TABLE IF EXISTS `apps_campaignanalysis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_campaignanalysis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(45) NOT NULL,
  `status_id` varchar(45) NOT NULL,
  `status_message` longtext DEFAULT NULL,
  `status_published` datetime(6) NOT NULL,
  `num_reactions` int(11) NOT NULL,
  `num_comments` int(11) NOT NULL,
  `num_shares` int(11) NOT NULL,
  `num_likes` int(11) NOT NULL,
  `num_loves` int(11) NOT NULL,
  `num_wows` int(11) NOT NULL,
  `num_hahas` int(11) NOT NULL,
  `num_sads` int(11) NOT NULL,
  `num_angrys` int(11) NOT NULL,
  `fans_comments` int(11) NOT NULL,
  `author_comments` int(11) NOT NULL,
  `total_comments` int(11) NOT NULL,
  `response_ration` double NOT NULL,
  `negative` int(11) NOT NULL,
  `positive` int(11) NOT NULL,
  `neutral` int(11) NOT NULL,
  `ratiopos` double NOT NULL,
  `rationeg` double NOT NULL,
  `rationeu` double NOT NULL,
  `query` int(11) NOT NULL,
  `complain` int(11) NOT NULL,
  `appreciation` int(11) NOT NULL,
  `feedback` int(11) NOT NULL,
  `spam` int(11) NOT NULL,
  `wom` int(11) NOT NULL,
  `query_percentage` double NOT NULL,
  `complain_percentage` double NOT NULL,
  `appreciation_percentage` double NOT NULL,
  `feedback_percentage` double NOT NULL,
  `spam_percentage` double NOT NULL,
  `wom_percentage` double NOT NULL,
  `pi` int(11) NOT NULL,
  `pi_percentage` double NOT NULL,
  `product_service` int(11) NOT NULL,
  `after_sales` int(11) NOT NULL,
  `campaign_offers` int(11) NOT NULL,
  `others` int(11) NOT NULL,
  `product_service_percentage` double NOT NULL,
  `after_sales_percentage` double NOT NULL,
  `campaign_offers_percentage` double NOT NULL,
  `others_percentage` double NOT NULL,
  `dropout_percentage` double NOT NULL,
  `post_user_id` int(11) NOT NULL,
  `as_appreciation` int(11) NOT NULL,
  `as_complain` int(11) NOT NULL,
  `as_neg` int(11) NOT NULL,
  `as_neg_fed` int(11) NOT NULL,
  `as_neu` int(11) NOT NULL,
  `as_pos` int(11) NOT NULL,
  `as_pos_fed` int(11) NOT NULL,
  `as_query` int(11) NOT NULL,
  `cf_neg` int(11) NOT NULL,
  `cf_neu` int(11) NOT NULL,
  `cf_pos` int(11) NOT NULL,
  `co_appreciation` int(11) NOT NULL,
  `co_complain` int(11) NOT NULL,
  `co_neg_fed` int(11) NOT NULL,
  `co_pos_fed` int(11) NOT NULL,
  `co_query` int(11) NOT NULL,
  `ot_appreciation` int(11) NOT NULL,
  `ot_complain` int(11) NOT NULL,
  `ot_neg` int(11) NOT NULL,
  `ot_neg_fed` int(11) NOT NULL,
  `ot_neu` int(11) NOT NULL,
  `ot_pos` int(11) NOT NULL,
  `ot_pos_fed` int(11) NOT NULL,
  `ot_query` int(11) NOT NULL,
  `ps_appreciation` int(11) NOT NULL,
  `ps_complain` int(11) NOT NULL,
  `ps_neg` int(11) NOT NULL,
  `ps_neg_fed` int(11) NOT NULL,
  `ps_neu` int(11) NOT NULL,
  `ps_pos` int(11) NOT NULL,
  `ps_pos_fed` int(11) NOT NULL,
  `ps_query` int(11) NOT NULL,
  `total_as` int(11) NOT NULL,
  `total_co` int(11) NOT NULL,
  `total_ot` int(11) NOT NULL,
  `total_ps` int(11) NOT NULL,
  `postimg` longtext DEFAULT NULL,
  `npi` int(11) NOT NULL,
  `campaign_status` int(11) NOT NULL,
  `analysis_time` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `apps_campaignanalysis_post_user_id_3ad6b381_fk_auth_user_id` (`post_user_id`),
  KEY `idx_apps_campaignanalysis_company_status_id` (`company`,`status_id`),
  CONSTRAINT `apps_campaignanalysis_post_user_id_3ad6b381_fk_auth_user_id` FOREIGN KEY (`post_user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_campaignanalysis`
--

LOCK TABLES `apps_campaignanalysis` WRITE;
/*!40000 ALTER TABLE `apps_campaignanalysis` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_campaignanalysis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_campaignanalysisdaterange`
--

DROP TABLE IF EXISTS `apps_campaignanalysisdaterange`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_campaignanalysisdaterange` (
  `company` varchar(45) NOT NULL,
  `daterange` varchar(45) NOT NULL,
  `num_reactions` int(11) NOT NULL,
  `num_comments` int(11) NOT NULL,
  `num_shares` int(11) NOT NULL,
  `num_likes` int(11) NOT NULL,
  `num_loves` int(11) NOT NULL,
  `num_wows` int(11) NOT NULL,
  `num_hahas` int(11) NOT NULL,
  `num_sads` int(11) NOT NULL,
  `num_angrys` int(11) NOT NULL,
  `fans_comments` int(11) NOT NULL,
  `author_comments` int(11) NOT NULL,
  `total_comments` int(11) NOT NULL,
  `response_ration` double NOT NULL,
  `negative` int(11) NOT NULL,
  `positive` int(11) NOT NULL,
  `neutral` int(11) NOT NULL,
  `ratiopos` double NOT NULL,
  `rationeg` double NOT NULL,
  `rationeu` double NOT NULL,
  `query` int(11) NOT NULL,
  `complain` int(11) NOT NULL,
  `appreciation` int(11) NOT NULL,
  `feedback` int(11) NOT NULL,
  `spam` int(11) NOT NULL,
  `wom` int(11) NOT NULL,
  `query_percentage` double NOT NULL,
  `complain_percentage` double NOT NULL,
  `appreciation_percentage` double NOT NULL,
  `feedback_percentage` double NOT NULL,
  `spam_percentage` double NOT NULL,
  `wom_percentage` double NOT NULL,
  `pi` int(11) NOT NULL,
  `pi_percentage` double NOT NULL,
  `product_service` int(11) NOT NULL,
  `after_sales` int(11) NOT NULL,
  `campaign_offers` int(11) NOT NULL,
  `others` int(11) NOT NULL,
  `product_service_percentage` double NOT NULL,
  `after_sales_percentage` double NOT NULL,
  `campaign_offers_percentage` double NOT NULL,
  `others_percentage` double NOT NULL,
  `dropout_percentage` double NOT NULL,
  `post_user_id` int(11) NOT NULL,
  `as_appreciation` int(11) NOT NULL,
  `as_complain` int(11) NOT NULL,
  `as_neg` int(11) NOT NULL,
  `as_neg_fed` int(11) NOT NULL,
  `as_neu` int(11) NOT NULL,
  `as_pos` int(11) NOT NULL,
  `as_pos_fed` int(11) NOT NULL,
  `as_query` int(11) NOT NULL,
  `cf_neg` int(11) NOT NULL,
  `cf_neu` int(11) NOT NULL,
  `cf_pos` int(11) NOT NULL,
  `co_appreciation` int(11) NOT NULL,
  `co_complain` int(11) NOT NULL,
  `co_neg_fed` int(11) NOT NULL,
  `co_pos_fed` int(11) NOT NULL,
  `co_query` int(11) NOT NULL,
  `npi` int(11) NOT NULL,
  `ot_appreciation` int(11) NOT NULL,
  `ot_complain` int(11) NOT NULL,
  `ot_neg` int(11) NOT NULL,
  `ot_neg_fed` int(11) NOT NULL,
  `ot_neu` int(11) NOT NULL,
  `ot_pos` int(11) NOT NULL,
  `ot_pos_fed` int(11) NOT NULL,
  `ot_query` int(11) NOT NULL,
  `postimg` longtext DEFAULT NULL,
  `ps_appreciation` int(11) NOT NULL,
  `ps_complain` int(11) NOT NULL,
  `ps_neg` int(11) NOT NULL,
  `ps_neg_fed` int(11) NOT NULL,
  `ps_neu` int(11) NOT NULL,
  `ps_pos` int(11) NOT NULL,
  `ps_pos_fed` int(11) NOT NULL,
  `ps_query` int(11) NOT NULL,
  `total_as` int(11) NOT NULL,
  `total_co` int(11) NOT NULL,
  `total_ot` int(11) NOT NULL,
  `total_ps` int(11) NOT NULL,
  `daterange_status` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_campaignanalysisdaterange`
--

LOCK TABLES `apps_campaignanalysisdaterange` WRITE;
/*!40000 ALTER TABLE `apps_campaignanalysisdaterange` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_campaignanalysisdaterange` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_comment`
--

DROP TABLE IF EXISTS `apps_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_message` longtext DEFAULT NULL,
  `status_id` varchar(45) NOT NULL,
  `negative` int(11) NOT NULL,
  `positive` int(11) NOT NULL,
  `neutral` int(11) NOT NULL,
  `model_sent` int(11) NOT NULL,
  `query` int(11) NOT NULL,
  `complain` int(11) NOT NULL,
  `appreciation` int(11) NOT NULL,
  `feedback` int(11) NOT NULL,
  `feedbackneg` int(11) NOT NULL,
  `feedbackpos` int(11) NOT NULL,
  `spam` int(11) NOT NULL,
  `wom` int(11) NOT NULL,
  `model_com` int(11) NOT NULL,
  `pi` int(11) NOT NULL,
  `npi` int(11) NOT NULL,
  `notpi` int(11) NOT NULL,
  `model_int` int(11) NOT NULL,
  `product_service` int(11) NOT NULL,
  `after_sales` int(11) NOT NULL,
  `campaign_offers` int(11) NOT NULL,
  `others` int(11) NOT NULL,
  `model_cat` int(11) NOT NULL,
  `comment_author` varchar(45) NOT NULL,
  `comment_likes` varchar(45) NOT NULL,
  `author_url` varchar(45) NOT NULL,
  `comment_id` varchar(45) NOT NULL,
  `company` varchar(45) NOT NULL,
  `comment_published` datetime NOT NULL,
  `misclassified` int(11) NOT NULL,
  `comment_status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `apps_comment_company_index` (`company`),
  KEY `apps_comment_status_id_index` (`status_id`),
  KEY `apps_comment_company_authorurl_index` (`company`,`author_url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_comment`
--

LOCK TABLES `apps_comment` WRITE;
/*!40000 ALTER TABLE `apps_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_commentatorprofile`
--

DROP TABLE IF EXISTS `apps_commentatorprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_commentatorprofile` (
  `socialid` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `totoal_comment` int(11) DEFAULT NULL,
  `pur_pattern` decimal(5,2) DEFAULT NULL,
  `spam` int(11) DEFAULT NULL,
  `score` decimal(5,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_commentatorprofile`
--

LOCK TABLES `apps_commentatorprofile` WRITE;
/*!40000 ALTER TABLE `apps_commentatorprofile` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_commentatorprofile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_daterangecomment`
--

DROP TABLE IF EXISTS `apps_daterangecomment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_daterangecomment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_message` longtext DEFAULT NULL,
  `status_id` varchar(45) NOT NULL,
  `negative` int(11) NOT NULL,
  `positive` int(11) NOT NULL,
  `neutral` int(11) NOT NULL,
  `model_sent` int(11) NOT NULL,
  `query` int(11) NOT NULL,
  `complain` int(11) NOT NULL,
  `appreciation` int(11) NOT NULL,
  `feedbackneg` int(11) NOT NULL,
  `feedbackpos` int(11) NOT NULL,
  `wom` int(11) NOT NULL,
  `spam` int(11) NOT NULL,
  `model_com` int(11) NOT NULL,
  `pi` int(11) NOT NULL,
  `npi` int(11) NOT NULL,
  `notpi` int(11) NOT NULL,
  `model_int` int(11) NOT NULL,
  `product_service` int(11) NOT NULL,
  `after_sales` int(11) NOT NULL,
  `campaign_offers` int(11) NOT NULL,
  `others` int(11) NOT NULL,
  `model_cat` int(11) NOT NULL,
  `comment_author` varchar(45) NOT NULL,
  `comment_published` datetime NOT NULL,
  `comment_likes` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_daterangecomment`
--

LOCK TABLES `apps_daterangecomment` WRITE;
/*!40000 ALTER TABLE `apps_daterangecomment` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_daterangecomment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_daterangenewurl`
--

DROP TABLE IF EXISTS `apps_daterangenewurl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_daterangenewurl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `user` varchar(45) NOT NULL,
  `comName` varchar(45) NOT NULL,
  `urltime` datetime(6) NOT NULL,
  `startdate` datetime(6) NOT NULL,
  `enddate` datetime(6) NOT NULL,
  `statusid` varchar(45) NOT NULL,
  `ugroup_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `apps_daterangenewurl_ugroup_id_8978f537_fk_apps_usergrouplist_id` (`ugroup_id`),
  CONSTRAINT `apps_daterangenewurl_ugroup_id_8978f537_fk_apps_usergrouplist_id` FOREIGN KEY (`ugroup_id`) REFERENCES `apps_usergrouplist` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_daterangenewurl`
--

LOCK TABLES `apps_daterangenewurl` WRITE;
/*!40000 ALTER TABLE `apps_daterangenewurl` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_daterangenewurl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_daterangepost`
--

DROP TABLE IF EXISTS `apps_daterangepost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_daterangepost` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(45) NOT NULL,
  `status_id` varchar(45) NOT NULL,
  `num_reactions` int(11) NOT NULL,
  `num_comments` int(11) NOT NULL,
  `num_shares` int(11) NOT NULL,
  `num_likes` int(11) NOT NULL,
  `num_loves` int(11) NOT NULL,
  `num_wows` int(11) NOT NULL,
  `num_hahas` int(11) NOT NULL,
  `num_sads` int(11) NOT NULL,
  `num_angrys` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `fans_comments` int(11) NOT NULL,
  `author_comments` int(11) NOT NULL,
  `total_comments` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_daterangepost`
--

LOCK TABLES `apps_daterangepost` WRITE;
/*!40000 ALTER TABLE `apps_daterangepost` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_daterangepost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_facebookpagedetails`
--

DROP TABLE IF EXISTS `apps_facebookpagedetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_facebookpagedetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `compnay_name` varchar(45) NOT NULL,
  `logo` longtext DEFAULT NULL,
  `page_name` varchar(45) NOT NULL,
  `page_id` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_facebookpagedetails`
--

LOCK TABLES `apps_facebookpagedetails` WRITE;
/*!40000 ALTER TABLE `apps_facebookpagedetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_facebookpagedetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_featurepermission`
--

DROP TABLE IF EXISTS `apps_featurepermission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_featurepermission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_analysis` tinyint(1) NOT NULL,
  `post_reactions` tinyint(1) NOT NULL,
  `post_comments_count` tinyint(1) NOT NULL,
  `post_shares_count` tinyint(1) NOT NULL,
  `post_response_ratio` tinyint(1) NOT NULL,
  `post_top_comments` tinyint(1) NOT NULL,
  `post_business` tinyint(1) NOT NULL,
  `post_consumer` tinyint(1) NOT NULL,
  `post_customer` tinyint(1) NOT NULL,
  `post_word` tinyint(1) NOT NULL,
  `post_domain` tinyint(1) NOT NULL,
  `post_divergence` tinyint(1) NOT NULL,
  `camp_analysis` tinyint(1) NOT NULL,
  `camp_reactions` tinyint(1) NOT NULL,
  `camp_comments_count` tinyint(1) NOT NULL,
  `camp_shares_count` tinyint(1) NOT NULL,
  `camp_response_ratio` tinyint(1) NOT NULL,
  `camp_top_comments` tinyint(1) NOT NULL,
  `camp_business` tinyint(1) NOT NULL,
  `camp_consumer` tinyint(1) NOT NULL,
  `camp_customer` tinyint(1) NOT NULL,
  `camp_word` tinyint(1) NOT NULL,
  `camp_domain` tinyint(1) NOT NULL,
  `camp_divergence` tinyint(1) NOT NULL,
  `time_analysis` tinyint(1) NOT NULL,
  `time_reactions` tinyint(1) NOT NULL,
  `time_comments_count` tinyint(1) NOT NULL,
  `time_shares_count` tinyint(1) NOT NULL,
  `time_response_ratio` tinyint(1) NOT NULL,
  `time_top_comments` tinyint(1) NOT NULL,
  `time_business` tinyint(1) NOT NULL,
  `time_consumer` tinyint(1) NOT NULL,
  `time_customer` tinyint(1) NOT NULL,
  `time_word` tinyint(1) NOT NULL,
  `time_domain` tinyint(1) NOT NULL,
  `time_divergence` tinyint(1) NOT NULL,
  `keyword_analysis` tinyint(1) NOT NULL,
  `keyword_reactions` tinyint(1) NOT NULL,
  `keyword_comments_count` tinyint(1) NOT NULL,
  `keyword_shares_count` tinyint(1) NOT NULL,
  `keyword_response_ratio` tinyint(1) NOT NULL,
  `keyword_top_comments` tinyint(1) NOT NULL,
  `keyword_business` tinyint(1) NOT NULL,
  `keyword_consumer` tinyint(1) NOT NULL,
  `keyword_customer` tinyint(1) NOT NULL,
  `keyword_word` tinyint(1) NOT NULL,
  `keyword_domain` tinyint(1) NOT NULL,
  `keyword_divergence` tinyint(1) NOT NULL,
  `report_generate` tinyint(1) NOT NULL,
  `report_pdf` tinyint(1) NOT NULL,
  `report_csv` tinyint(1) NOT NULL,
  `datatable` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_featurepermission`
--

LOCK TABLES `apps_featurepermission` WRITE;
/*!40000 ALTER TABLE `apps_featurepermission` DISABLE KEYS */;
INSERT INTO `apps_featurepermission` VALUES (1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1);
/*!40000 ALTER TABLE `apps_featurepermission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_gpprofile`
--

DROP TABLE IF EXISTS `apps_gpprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_gpprofile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `socialid` varchar(45) NOT NULL,
  `telcoid` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `totoal_comment` int(11) NOT NULL,
  `pur_pattern` int(11) NOT NULL,
  `spam` int(11) NOT NULL,
  `score` decimal(5,2) NOT NULL,
  `cumSum` decimal(5,2) NOT NULL,
  `sat_level` int(11) NOT NULL,
  `last_interection` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_gpprofile`
--

LOCK TABLES `apps_gpprofile` WRITE;
/*!40000 ALTER TABLE `apps_gpprofile` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_gpprofile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_keywordanalysis`
--

DROP TABLE IF EXISTS `apps_keywordanalysis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_keywordanalysis` (
  `company` varchar(45) NOT NULL,
  `daterange` varchar(45) NOT NULL,
  `keyword` longtext DEFAULT NULL,
  `response_ration` int(11) NOT NULL,
  `negative` int(11) NOT NULL,
  `positive` int(11) NOT NULL,
  `neutral` int(11) NOT NULL,
  `ratiopos` int(11) NOT NULL,
  `rationeg` int(11) NOT NULL,
  `rationeu` int(11) NOT NULL,
  `query` int(11) NOT NULL,
  `complain` int(11) NOT NULL,
  `appreciation` int(11) NOT NULL,
  `feedback` int(11) NOT NULL,
  `feedbackneg` int(11) NOT NULL,
  `feedbackpos` int(11) NOT NULL,
  `spam` int(11) NOT NULL,
  `wom` int(11) NOT NULL,
  `query_percentage` int(11) NOT NULL,
  `complain_percentage` int(11) NOT NULL,
  `appreciation_percentage` int(11) NOT NULL,
  `feedback_percentage` int(11) NOT NULL,
  `spam_percentage` int(11) NOT NULL,
  `wom_percentage` int(11) NOT NULL,
  `pi` int(11) NOT NULL,
  `npi` int(11) NOT NULL,
  `notpi` int(11) NOT NULL,
  `pi_percentage` int(11) NOT NULL,
  `product_service` int(11) NOT NULL,
  `after_sales` int(11) NOT NULL,
  `campaign_offers` int(11) NOT NULL,
  `others` int(11) NOT NULL,
  `product_service_percentage` int(11) NOT NULL,
  `after_sales_percentage` int(11) NOT NULL,
  `campaign_offers_percentage` int(11) NOT NULL,
  `others_percentage` int(11) NOT NULL,
  `dropout_percentage` int(11) NOT NULL,
  `comment_likes` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `ps_pos` int(11) NOT NULL,
  `ps_neg` int(11) NOT NULL,
  `ps_neu` int(11) NOT NULL,
  `ps_query` int(11) NOT NULL,
  `ps_complain` int(11) NOT NULL,
  `ps_appreciation` int(11) NOT NULL,
  `ps_neg_fed` int(11) NOT NULL,
  `ps_pos_fed` int(11) NOT NULL,
  `total_ps` int(11) NOT NULL,
  `as_pos` int(11) NOT NULL,
  `as_neg` int(11) NOT NULL,
  `as_neu` int(11) NOT NULL,
  `as_query` int(11) NOT NULL,
  `as_complain` int(11) NOT NULL,
  `as_appreciation` int(11) NOT NULL,
  `as_neg_fed` int(11) NOT NULL,
  `as_pos_fed` int(11) NOT NULL,
  `total_as` int(11) NOT NULL,
  `cf_pos` int(11) NOT NULL,
  `cf_neg` int(11) NOT NULL,
  `cf_neu` int(11) NOT NULL,
  `co_query` int(11) NOT NULL,
  `co_complain` int(11) NOT NULL,
  `co_appreciation` int(11) NOT NULL,
  `co_neg_fed` int(11) NOT NULL,
  `co_pos_fed` int(11) NOT NULL,
  `total_co` int(11) NOT NULL,
  `ot_pos` int(11) NOT NULL,
  `ot_neg` int(11) NOT NULL,
  `ot_neu` int(11) NOT NULL,
  `ot_query` int(11) NOT NULL,
  `ot_complain` int(11) NOT NULL,
  `ot_appreciation` int(11) NOT NULL,
  `ot_neg_fed` int(11) NOT NULL,
  `ot_pos_fed` int(11) NOT NULL,
  `total_ot` int(11) NOT NULL,
  `post_user_id` int(11) NOT NULL,
  `total_comments` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_keywordanalysis`
--

LOCK TABLES `apps_keywordanalysis` WRITE;
/*!40000 ALTER TABLE `apps_keywordanalysis` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_keywordanalysis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_keywordurl`
--

DROP TABLE IF EXISTS `apps_keywordurl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_keywordurl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `user` varchar(45) NOT NULL,
  `comName` varchar(45) NOT NULL,
  `urltime` datetime(6) NOT NULL,
  `startdate` datetime(6) NOT NULL,
  `enddate` datetime(6) NOT NULL,
  `statusid` varchar(45) NOT NULL,
  `keyword` varchar(45) NOT NULL,
  `ugroup_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `apps_keywordewurl_ugroup_id_5b1964dc_fk_apps_usergrouplist_id` (`ugroup_id`),
  CONSTRAINT `apps_keywordewurl_ugroup_id_5b1964dc_fk_apps_usergrouplist_id` FOREIGN KEY (`ugroup_id`) REFERENCES `apps_usergrouplist` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_keywordurl`
--

LOCK TABLES `apps_keywordurl` WRITE;
/*!40000 ALTER TABLE `apps_keywordurl` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_keywordurl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_misclassifieddatastore`
--

DROP TABLE IF EXISTS `apps_misclassifieddatastore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_misclassifieddatastore` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_ref_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_misclassifieddatastore`
--

LOCK TABLES `apps_misclassifieddatastore` WRITE;
/*!40000 ALTER TABLE `apps_misclassifieddatastore` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_misclassifieddatastore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_newurl`
--

DROP TABLE IF EXISTS `apps_newurl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_newurl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comName` varchar(45) NOT NULL,
  `urltime` datetime(6) NOT NULL,
  `statusid` varchar(45) NOT NULL,
  `post_id` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `apps_newurl_user_id_2d51ce25` (`user_id`),
  CONSTRAINT `apps_newurl_user_id_2d51ce25_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_newurl`
--

LOCK TABLES `apps_newurl` WRITE;
/*!40000 ALTER TABLE `apps_newurl` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_newurl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_post`
--

DROP TABLE IF EXISTS `apps_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(45) NOT NULL,
  `status_id` varchar(45) NOT NULL,
  `status_message` longtext DEFAULT NULL,
  `status_published` datetime(6) NOT NULL,
  `num_reactions` int(11) NOT NULL,
  `num_comments` int(11) NOT NULL,
  `num_shares` int(11) NOT NULL,
  `num_likes` int(11) NOT NULL,
  `num_loves` int(11) NOT NULL,
  `num_wows` int(11) NOT NULL,
  `num_hahas` int(11) NOT NULL,
  `num_sads` int(11) NOT NULL,
  `num_angrys` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `fans_comments` int(11) NOT NULL,
  `author_comments` int(11) NOT NULL,
  `total_comments` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `posturl` varchar(255) NOT NULL,
  `postimg` longtext DEFAULT NULL,
  `post_status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `apps_post_user_id_03b5f921_fk_auth_user_id` (`user_id`),
  KEY `idx_apps_post_status_id` (`status_id`),
  CONSTRAINT `apps_post_user_id_03b5f921_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_post`
--

LOCK TABLES `apps_post` WRITE;
/*!40000 ALTER TABLE `apps_post` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_postlog`
--

DROP TABLE IF EXISTS `apps_postlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_postlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_postlog`
--

LOCK TABLES `apps_postlog` WRITE;
/*!40000 ALTER TABLE `apps_postlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_postlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_requestdemo`
--

DROP TABLE IF EXISTS `apps_requestdemo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_requestdemo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `email` varchar(254) NOT NULL,
  `contactnum` varchar(45) NOT NULL,
  `organization` varchar(45) NOT NULL,
  `message` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_requestdemo`
--

LOCK TABLES `apps_requestdemo` WRITE;
/*!40000 ALTER TABLE `apps_requestdemo` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_requestdemo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_robiprofile`
--

DROP TABLE IF EXISTS `apps_robiprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_robiprofile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `socialid` varchar(45) NOT NULL,
  `telcoid` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `totoal_comment` int(11) NOT NULL,
  `pur_pattern` int(11) NOT NULL,
  `spam` int(11) NOT NULL,
  `score` decimal(5,2) NOT NULL,
  `cumSum` decimal(5,2) NOT NULL,
  `sat_level` int(11) NOT NULL,
  `last_interection` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_apps_robiprofile_socialid` (`socialid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_robiprofile`
--

LOCK TABLES `apps_robiprofile` WRITE;
/*!40000 ALTER TABLE `apps_robiprofile` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_robiprofile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_teleprofile`
--

DROP TABLE IF EXISTS `apps_teleprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_teleprofile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `socialid` varchar(45) NOT NULL,
  `telcoid` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `totoal_comment` int(11) NOT NULL,
  `pur_pattern` int(11) NOT NULL,
  `spam` int(11) NOT NULL,
  `score` decimal(5,2) NOT NULL,
  `cumSum` decimal(5,2) NOT NULL,
  `sat_level` int(11) NOT NULL,
  `last_interection` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_apps_teleprofile_socialid` (`socialid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_teleprofile`
--

LOCK TABLES `apps_teleprofile` WRITE;
/*!40000 ALTER TABLE `apps_teleprofile` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_teleprofile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_twofactorauth`
--

DROP TABLE IF EXISTS `apps_twofactorauth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_twofactorauth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uname` varchar(45) NOT NULL,
  `upass` varchar(45) NOT NULL,
  `logintime` datetime(6) NOT NULL,
  `sendkeys` varchar(45) NOT NULL,
  `ipaddress` varchar(45) NOT NULL,
  `sendattamp` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_twofactorauth`
--

LOCK TABLES `apps_twofactorauth` WRITE;
/*!40000 ALTER TABLE `apps_twofactorauth` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_twofactorauth` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_userdetails`
--

DROP TABLE IF EXISTS `apps_userdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_userdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `profilepic` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `apps_userdetails_user_id_de7b028f_fk_auth_user_id` (`user_id`),
  CONSTRAINT `apps_userdetails_user_id_de7b028f_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_userdetails`
--

LOCK TABLES `apps_userdetails` WRITE;
/*!40000 ALTER TABLE `apps_userdetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_userdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_usergrouplist`
--

DROP TABLE IF EXISTS `apps_usergrouplist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_usergrouplist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `facebook_name` varchar(45) NOT NULL,
  `profile_img` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_usergrouplist`
--

LOCK TABLES `apps_usergrouplist` WRITE;
/*!40000 ALTER TABLE `apps_usergrouplist` DISABLE KEYS */;
INSERT INTO `apps_usergrouplist` VALUES (1,'Grameenphone','Grameenphone','default.jpg'),(2,'banglalinkmela','Banglalink Digital','default.jpg'),(3,'airtelbuzz','Airtel Buzz','default.jpg'),(4,'RobiFanz','Robi Axiata Limited','default.jpg'),(5,'yourTELETALK','Teletalk','default.jpg'),(6,'Demo','Demo','default.jpg'),(7,'SamsungBangladesh','Samsung','default.jpg'),(8,'PranFrooto','PRAN Frooto','default.jpg'),(9,'VaselineBangladesh','Vaseline','default.jpg'),(10,'symphonymobile','Symphony Mobile','default.jpg'),(11,'fairandlovelybd','Fair & Lovely Bangladesh','default.jpg'),(12,'CloseUpBangladesh','Closeup Bangladesh','default.jpg'),(13,'CocaColaBGD','Coca-Cola Bangladesh','default.jpg'),(14,'fairandlovelybd','Fair & Lovely Bangladesh','default.jpg');
/*!40000 ALTER TABLE `apps_usergrouplist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_usergrouppermission`
--

DROP TABLE IF EXISTS `apps_usergrouppermission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_usergrouppermission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupadmin` varchar(45) NOT NULL,
  `limit` varchar(45) NOT NULL,
  `staus` varchar(45) NOT NULL,
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `apps_usergrouppermis_group_id_dc1d95ed_fk_apps_user` (`group_id`),
  KEY `apps_usergrouppermission_user_id_13084b1c_fk_auth_user_id` (`user_id`),
  CONSTRAINT `apps_usergrouppermis_group_id_dc1d95ed_fk_apps_user` FOREIGN KEY (`group_id`) REFERENCES `apps_usergrouplist` (`id`),
  CONSTRAINT `apps_usergrouppermission_user_id_13084b1c_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_usergrouppermission`
--

LOCK TABLES `apps_usergrouppermission` WRITE;
/*!40000 ALTER TABLE `apps_usergrouppermission` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_usergrouppermission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apps_userlog`
--

DROP TABLE IF EXISTS `apps_userlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apps_userlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `todaydate` datetime(6) NOT NULL,
  `logintime` datetime(6) NOT NULL,
  `logouttime` datetime(6) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `urlreq` int(11) NOT NULL,
  `ugroup_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `apps_userlog_ugroup_id_994fe6a8_fk_apps_usergrouplist_id` (`ugroup_id`),
  KEY `apps_userlog_user_id_3f6b6555_fk_auth_user_id` (`user_id`),
  CONSTRAINT `apps_userlog_ugroup_id_994fe6a8_fk_apps_usergrouplist_id` FOREIGN KEY (`ugroup_id`) REFERENCES `apps_usergrouplist` (`id`),
  CONSTRAINT `apps_userlog_user_id_3f6b6555_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apps_userlog`
--

LOCK TABLES `apps_userlog` WRITE;
/*!40000 ALTER TABLE `apps_userlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `apps_userlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add user',2,'add_user'),(5,'Can change user',2,'change_user'),(6,'Can delete user',2,'delete_user'),(7,'Can add permission',3,'add_permission'),(8,'Can change permission',3,'change_permission'),(9,'Can delete permission',3,'delete_permission'),(10,'Can add group',4,'add_group'),(11,'Can change group',4,'change_group'),(12,'Can delete group',4,'delete_group'),(13,'Can add site',5,'add_site'),(14,'Can change site',5,'change_site'),(15,'Can delete site',5,'delete_site'),(16,'Can add content type',6,'add_contenttype'),(17,'Can change content type',6,'change_contenttype'),(18,'Can delete content type',6,'delete_contenttype'),(19,'Can add session',7,'add_session'),(20,'Can change session',7,'change_session'),(21,'Can delete session',7,'delete_session'),(22,'Can add comment',8,'add_comment'),(23,'Can change comment',8,'change_comment'),(24,'Can delete comment',8,'delete_comment'),(25,'Can add date range new url',9,'add_daterangenewurl'),(26,'Can change date range new url',9,'change_daterangenewurl'),(27,'Can delete date range new url',9,'delete_daterangenewurl'),(28,'Can add air profile',10,'add_airprofile'),(29,'Can change air profile',10,'change_airprofile'),(30,'Can delete air profile',10,'delete_airprofile'),(31,'Can add bl profile',11,'add_blprofile'),(32,'Can change bl profile',11,'change_blprofile'),(33,'Can delete bl profile',11,'delete_blprofile'),(34,'Can add user log',12,'add_userlog'),(35,'Can change user log',12,'change_userlog'),(36,'Can delete user log',12,'delete_userlog'),(37,'Can add user group list',13,'add_usergrouplist'),(38,'Can change user group list',13,'change_usergrouplist'),(39,'Can delete user group list',13,'delete_usergrouplist'),(40,'Can add date range post',14,'add_daterangepost'),(41,'Can change date range post',14,'change_daterangepost'),(42,'Can delete date range post',14,'delete_daterangepost'),(43,'Can add post',15,'add_post'),(44,'Can change post',15,'change_post'),(45,'Can delete post',15,'delete_post'),(46,'Can add commentator profile',16,'add_commentatorprofile'),(47,'Can change commentator profile',16,'change_commentatorprofile'),(48,'Can delete commentator profile',16,'delete_commentatorprofile'),(49,'Can add robi profile',17,'add_robiprofile'),(50,'Can change robi profile',17,'change_robiprofile'),(51,'Can delete robi profile',17,'delete_robiprofile'),(52,'Can add campaign analysis',18,'add_campaignanalysis'),(53,'Can change campaign analysis',18,'change_campaignanalysis'),(54,'Can delete campaign analysis',18,'delete_campaignanalysis'),(55,'Can add commentator',19,'add_commentator'),(56,'Can change commentator',19,'change_commentator'),(57,'Can delete commentator',19,'delete_commentator'),(58,'Can add tele profile',20,'add_teleprofile'),(59,'Can change tele profile',20,'change_teleprofile'),(60,'Can delete tele profile',20,'delete_teleprofile'),(61,'Can add new url',21,'add_newurl'),(62,'Can change new url',21,'change_newurl'),(63,'Can delete new url',21,'delete_newurl'),(64,'Can add userdetails',22,'add_userdetails'),(65,'Can change userdetails',22,'change_userdetails'),(66,'Can delete userdetails',22,'delete_userdetails'),(67,'Can add gp profile',23,'add_gpprofile'),(68,'Can change gp profile',23,'change_gpprofile'),(69,'Can delete gp profile',23,'delete_gpprofile'),(70,'Can add request demo',24,'add_requestdemo'),(71,'Can change request demo',24,'change_requestdemo'),(72,'Can delete request demo',24,'delete_requestdemo'),(73,'Can add date range comment',25,'add_daterangecomment'),(74,'Can change date range comment',25,'change_daterangecomment'),(75,'Can delete date range comment',25,'delete_daterangecomment'),(76,'Can add user group permission',26,'add_usergrouppermission'),(77,'Can change user group permission',26,'change_usergrouppermission'),(78,'Can delete user group permission',26,'delete_usergrouppermission'),(79,'Can add email address',27,'add_emailaddress'),(80,'Can change email address',27,'change_emailaddress'),(81,'Can delete email address',27,'delete_emailaddress'),(82,'Can add email confirmation',28,'add_emailconfirmation'),(83,'Can change email confirmation',28,'change_emailconfirmation'),(84,'Can delete email confirmation',28,'delete_emailconfirmation'),(85,'Can add social application token',29,'add_socialtoken'),(86,'Can change social application token',29,'change_socialtoken'),(87,'Can delete social application token',29,'delete_socialtoken'),(88,'Can add social application',30,'add_socialapp'),(89,'Can change social application',30,'change_socialapp'),(90,'Can delete social application',30,'delete_socialapp'),(91,'Can add social account',31,'add_socialaccount'),(92,'Can change social account',31,'change_socialaccount'),(93,'Can delete social account',31,'delete_socialaccount'),(94,'Can add mis classified data store',32,'add_misclassifieddatastore'),(95,'Can change mis classified data store',32,'change_misclassifieddatastore'),(96,'Can delete mis classified data store',32,'delete_misclassifieddatastore'),(97,'Can add post log',33,'add_postlog'),(98,'Can change post log',33,'change_postlog'),(99,'Can delete post log',33,'delete_postlog'),(100,'Can add campaign analysis date range',34,'add_campaignanalysisdaterange'),(101,'Can change campaign analysis date range',34,'change_campaignanalysisdaterange'),(102,'Can delete campaign analysis date range',34,'delete_campaignanalysisdaterange'),(103,'Can add keyword url',35,'add_keywordurl'),(104,'Can change keyword url',35,'change_keywordurl'),(105,'Can delete keyword url',35,'delete_keywordurl'),(106,'Can add keyword analysis',36,'add_keywordanalysis'),(107,'Can change keyword analysis',36,'change_keywordanalysis'),(108,'Can delete keyword analysis',36,'delete_keywordanalysis'),(109,'Can add feature permission',37,'add_featurepermission'),(110,'Can change feature permission',37,'change_featurepermission'),(111,'Can delete feature permission',37,'delete_featurepermission'),(112,'Can add agency',38,'add_agency'),(113,'Can change agency',38,'change_agency'),(114,'Can delete agency',38,'delete_agency'),(115,'Can add agencyuserlist',39,'add_agencyuserlist'),(116,'Can change agencyuserlist',39,'change_agencyuserlist'),(117,'Can delete agencyuserlist',39,'delete_agencyuserlist'),(118,'Can add agency client',40,'add_agencyclient'),(119,'Can change agency client',40,'change_agencyclient'),(120,'Can delete agency client',40,'delete_agencyclient'),(121,'Can add two factor auth',41,'add_twofactorauth'),(122,'Can change two factor auth',41,'change_twofactorauth'),(123,'Can delete two factor auth',41,'delete_twofactorauth'),(124,'Can add TOTP device',42,'add_totpdevice'),(125,'Can change TOTP device',42,'change_totpdevice'),(126,'Can delete TOTP device',42,'delete_totpdevice'),(127,'Can add HOTP device',43,'add_hotpdevice'),(128,'Can change HOTP device',43,'change_hotpdevice'),(129,'Can delete HOTP device',43,'delete_hotpdevice'),(130,'Can add static device',44,'add_staticdevice'),(131,'Can change static device',44,'change_staticdevice'),(132,'Can delete static device',44,'delete_staticdevice'),(133,'Can add static token',45,'add_statictoken'),(134,'Can change static token',45,'change_statictoken'),(135,'Can delete static token',45,'delete_statictoken'),(136,'Can add facebook page details',46,'add_facebookpagedetails'),(137,'Can change facebook page details',46,'change_facebookpagedetails'),(138,'Can delete facebook page details',46,'delete_facebookpagedetails'),(139,'Can add phone device',47,'add_phonedevice'),(140,'Can change phone device',47,'change_phonedevice'),(141,'Can delete phone device',47,'delete_phonedevice'),(142,'Can add Remote YubiKey device',48,'add_remoteyubikeydevice'),(143,'Can change Remote YubiKey device',48,'change_remoteyubikeydevice'),(144,'Can delete Remote YubiKey device',48,'delete_remoteyubikeydevice'),(145,'Can add YubiKey validation service',49,'add_validationservice'),(146,'Can change YubiKey validation service',49,'change_validationservice'),(147,'Can delete YubiKey validation service',49,'delete_validationservice'),(148,'Can add Local YubiKey device',50,'add_yubikeydevice'),(149,'Can change Local YubiKey device',50,'change_yubikeydevice'),(150,'Can delete Local YubiKey device',50,'delete_yubikeydevice');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$36000$Fe9We1EthimU$Ao3FGRO4DsIv2V7A1jDbWyOB7k516PO6BybGU6SNPrI=','2018-05-11 04:40:01.258286',1,'admin','Mr. Admin','','shariful@datashall.com',1,1,'2017-12-14 15:55:34.386204'),(2,'pbkdf2_sha256$36000$kwTKSzGseu4M$AkClslvCgeU070rS/6hwZhF0LG4IheF9pyLyXv0EEGc=','2017-12-25 01:37:04.076013',0,'demo','',' ','demo@datashall.com',1,1,'2017-12-14 18:42:03.664648'),(3,'pbkdf2_sha256$36000$cMKI2L1bChjU$petJGCNkO6g+l6Hk6+HvlEdj9jOrzaKsGduV3DmvyPQ=','2017-12-23 11:27:40.971056',0,'demo2','',' ','demo2@datashall.com',0,1,'2017-12-23 11:27:00.117310'),(4,'pbkdf2_sha256$36000$4eZz8Br5zLpv$X0ip2vERQCO/Uklta2l7O7NFiWtSlMrHPBuS34LvDTo=','2017-12-25 00:26:11.303107',0,'demo3','','','demo3@datashall.com',0,1,'2017-12-24 17:58:26.815308'),(28,'pbkdf2_sha256$36000$7X7Rxnxp4RYB$6ITcExPvjSFnnZoCSPt8iaTlng3qcg0LCRvUu6TmaRo=','2018-05-10 10:18:41.799665',0,'shariful','Shariful Islam Nayon','','shariful@datashall.com',0,1,'2018-04-16 19:15:50.077295'),(30,'pbkdf2_sha256$36000$znPbhiTSOBCm$c9nyuaFvq0HmyeWb3v8QH+J4jGu/E9mqN1Ro0XFP+I0=','2018-05-02 18:22:21.626089',0,'farsi_back','salman ','','farsi_lkskdfj@datashall.com',0,0,'2018-04-18 17:06:49.294806'),(40,'pbkdf2_sha256$36000$gnDKboOCFaCM$+j9EPq5Ev3mA7v5cjSWQgZ0DPF5g8HmdVWYAtk2wgVI=',NULL,0,'haydar1','salman haydar','','haydar@data.com',0,0,'2018-04-23 15:56:55.598886'),(41,'pbkdf2_sha256$36000$77hfGqqR2WIC$h/a94VynIgmmQQ7eSdfX1AFCOrD+hsrNZtCgQAaMRc8=','2018-04-23 16:02:45.228364',0,'haydar','salman haydar','','haydar@datashall.com',0,1,'2018-04-23 16:01:12.891801'),(42,'pbkdf2_sha256$36000$BwMHPFwEI1du$WflX6vn9JrjqpRb4w0bzdH3NkxlQVIWe2pJUpFmNbiI=','2018-05-10 10:26:06.292959',0,'farsi21_back','salman farsi','','salman.farsi01_back@northsouth.edu',0,1,'2018-04-23 10:04:50.854096'),(43,'pbkdf2_sha256$36000$1jEgaodA88Lj$9UGMEhX4/rpwTWezT0qfASrqVAlpiucW6WIisYdh7UA=','2018-05-09 09:59:45.417218',0,'arefin','arefin','','arefin@datashall.com',0,1,'2018-04-23 10:07:56.836706'),(44,'pbkdf2_sha256$36000$MjWs8fxjp50W$OI6JskvIE8K2C4wZT/ne3WQyQYmPUuAGA6WYESzpVWY=',NULL,0,'rafiul','rafiul alam','','rafiul@datashall.com',0,0,'2018-04-23 10:19:17.197899'),(45,'pbkdf2_sha256$36000$2ZogZhT3nY2c$mIDA5e5I1HiPYiRM3pPC0sPxmvfCfQw6V80e+UAqBao=','2018-05-08 06:28:07.670077',0,'uzzal','uzzal','','uzzal@datashallcom',0,1,'2018-04-24 13:47:42.324752'),(46,'pbkdf2_sha256$36000$EPUWxByML7XF$pjthYakXyJlKPd2QZK6yhZaLT5g8uRTDMk2SCatZtwg=','2018-04-29 11:54:40.630027',0,'Shuni2','Yomamajokes','','arefin.corporate@gmail.com',0,1,'2018-04-29 11:30:54.323881'),(47,'pbkdf2_sha256$36000$guBrAtYZOafM$UGriUhvjaD+NUWLbqjKALclG2ZPI2HzRNEcha/L6wi0=',NULL,0,'demotest','Demon','','demotest@datashall.com',0,0,'2018-05-01 17:52:37.286518'),(48,'pbkdf2_sha256$36000$UEqhsWG3wuRj$Upy60So50EzMeuagVL3Peww9577Gr1FMerocb+gQ2rg=','2018-05-10 15:59:18.955864',0,'moin','Moin U.','','moin@datashall.com',0,1,'2018-05-02 07:45:15.873991'),(49,'pbkdf2_sha256$36000$0zS5ZJgPrbOY$p5yS1WdFGrD6ARvFkgWxlMlrjdWJJCifrui9B/ZGs+s=','2018-05-02 09:18:18.918856',0,'farsi01','salman farsi','','salman.farsi01@northsouth.edu',0,1,'2018-05-02 09:17:20.874491'),(50,'pbkdf2_sha256$36000$D8IKwdI9qrx9$xZ29xy1dXm/+MV8HbdeA7Tal0Y5/nuaSxSUHpoZMu8Y=','2018-05-10 12:03:40.031966',0,'alex','Mr. Alex','','moin.u.siddique@gmail.com',0,1,'2018-05-02 09:51:02.567385'),(51,'pbkdf2_sha256$36000$nBT3C194jjHM$gOvLB4EI5WGZZU6Igl7BhOtjYvy9eGzNUdaWGUvCXO4=',NULL,0,'sakib','Sakib Al Hassan','','recruitment@datashall.com',0,0,'2018-05-02 12:14:49.713390'),(52,'pbkdf2_sha256$36000$ZXRn7gONU70D$TvZpEMZdJaMPRH+d3h47WiOaeaHaOaK4EqAOuw5A8ao=','2018-05-02 12:18:41.536142',0,'mashrafee','Mortuza','','career@datashall.com',0,1,'2018-05-02 12:18:24.227747'),(53,'pbkdf2_sha256$36000$gALjUgPYrk6w$nr+sQdxxu3Dm+m0qF2ABHP9hXdfBJj8Ywco7ILuyfNU=',NULL,0,'salman_haydar','Haydar','','salman@datashall.com',0,0,'2018-05-03 07:00:07.337004'),(54,'pbkdf2_sha256$36000$bzHdGENGbz4u$Tc/0Sul2L15DXyXvmajvTxoh8fclw+kORqonqMgcSmU=',NULL,0,'farsi_hgfhg','Farsi','','ytrytfarsi@datashall.com',0,0,'2018-05-03 14:22:02.333739'),(55,'pbkdf2_sha256$36000$pTlpY5sMcSan$0N0f3OEsJ9uZ+ZF5KaZXo3wQ6HdoZ/WyttR/bAeUT/g=',NULL,0,'farsi_ytyt','Farsi','','jhgjfarsi@datashall.com',0,0,'2018-05-03 14:28:16.202608'),(56,'pbkdf2_sha256$36000$R82pMhxdS9G8$msdsvuLwsjPDiW15vKiMGGuuJzXHOfqc/whfhE3E8G4=','2018-05-08 09:26:41.150341',0,'farsi','Farsi','','farsi@datashall.com',0,1,'2018-05-03 14:31:05.232740'),(57,'pbkdf2_sha256$36000$KlZ7JK5ERG5v$tNZfuIUDxd4d++aw2uCqPleB6jq9mJthf+3dCQ09vJY=','2018-05-03 14:48:57.748495',0,'Numan','Data','','bmrafiul.alam@gmail.com',0,1,'2018-05-03 14:47:28.214054'),(58,'pbkdf2_sha256$36000$4QSJKG5SsHLJ$Zgvjgeqs07HSDtnwgICf+XtnS8OhhOkX6bKyYSg4h/s=',NULL,0,'ajax','ajax fc','','ajax@fc.com',0,0,'2018-05-08 09:39:30.714054');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (27,'account','emailaddress'),(28,'account','emailconfirmation'),(1,'admin','logentry'),(38,'apps','agency'),(40,'apps','agencyclient'),(39,'apps','agencyuserlist'),(10,'apps','airprofile'),(11,'apps','blprofile'),(18,'apps','campaignanalysis'),(34,'apps','campaignanalysisdaterange'),(8,'apps','comment'),(19,'apps','commentator'),(16,'apps','commentatorprofile'),(25,'apps','daterangecomment'),(9,'apps','daterangenewurl'),(14,'apps','daterangepost'),(46,'apps','facebookpagedetails'),(37,'apps','featurepermission'),(23,'apps','gpprofile'),(36,'apps','keywordanalysis'),(35,'apps','keywordurl'),(32,'apps','misclassifieddatastore'),(21,'apps','newurl'),(15,'apps','post'),(33,'apps','postlog'),(24,'apps','requestdemo'),(17,'apps','robiprofile'),(20,'apps','teleprofile'),(41,'apps','twofactorauth'),(22,'apps','userdetails'),(13,'apps','usergrouplist'),(26,'apps','usergrouppermission'),(12,'apps','userlog'),(4,'auth','group'),(3,'auth','permission'),(2,'auth','user'),(6,'contenttypes','contenttype'),(43,'otp_hotp','hotpdevice'),(44,'otp_static','staticdevice'),(45,'otp_static','statictoken'),(42,'otp_totp','totpdevice'),(48,'otp_yubikey','remoteyubikeydevice'),(49,'otp_yubikey','validationservice'),(50,'otp_yubikey','yubikeydevice'),(7,'sessions','session'),(5,'sites','site'),(31,'socialaccount','socialaccount'),(30,'socialaccount','socialapp'),(29,'socialaccount','socialtoken'),(47,'two_factor','phonedevice');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=162 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2017-12-14 15:54:38.482575'),(2,'auth','0001_initial','2017-12-14 15:54:38.649499'),(3,'account','0001_initial','2017-12-14 15:54:38.732635'),(4,'account','0002_email_max_length','2017-12-14 15:54:38.753925'),(5,'admin','0001_initial','2017-12-14 15:54:38.810876'),(6,'admin','0002_logentry_remove_auto_add','2017-12-14 15:54:38.829409'),(7,'apps','0001_initial','2017-12-14 15:54:39.248752'),(8,'apps','0002_auto_20171208_1152','2017-12-14 15:54:39.390535'),(9,'apps','0003_auto_20171213_2058','2017-12-14 15:54:39.550904'),(10,'contenttypes','0002_remove_content_type_name','2017-12-14 15:54:39.609240'),(11,'auth','0002_alter_permission_name_max_length','2017-12-14 15:54:39.627100'),(12,'auth','0003_alter_user_email_max_length','2017-12-14 15:54:39.646495'),(13,'auth','0004_alter_user_username_opts','2017-12-14 15:54:39.664650'),(14,'auth','0005_alter_user_last_login_null','2017-12-14 15:54:39.691589'),(15,'auth','0006_require_contenttypes_0002','2017-12-14 15:54:39.694844'),(16,'auth','0007_alter_validators_add_error_messages','2017-12-14 15:54:39.711845'),(17,'auth','0008_alter_user_username_max_length','2017-12-14 15:54:39.742118'),(18,'sessions','0001_initial','2017-12-14 15:54:39.758900'),(19,'sites','0001_initial','2017-12-14 15:54:39.770654'),(20,'sites','0002_alter_domain_unique','2017-12-14 15:54:39.779761'),(21,'socialaccount','0001_initial','2017-12-14 15:54:39.949268'),(22,'socialaccount','0002_token_max_lengths','2017-12-14 15:54:39.994015'),(23,'socialaccount','0003_extra_data_default_dict','2017-12-14 15:54:40.010418'),(24,'apps','0002_auto_20180123_0602','2018-02-06 13:57:08.248483'),(25,'apps','0003_keywordewurl','2018-02-06 13:57:09.363237'),(26,'apps','0004_auto_20180127_1909','2018-02-06 13:57:09.691530'),(27,'apps','0005_auto_20180129_1715','2018-02-10 14:27:47.767371'),(28,'apps','0006_auto_20180129_1946','2018-02-10 14:27:48.729211'),(29,'apps','0007_auto_20180204_1337','2018-02-10 14:27:50.403457'),(30,'apps','0002_auto_20180210_1439','2018-02-10 14:39:18.092014'),(31,'apps','0003_auto_20180210_1439','2018-02-10 14:39:34.468062'),(32,'apps','0004_auto_20180210_1439','2018-02-10 15:35:28.194330'),(33,'apps','0005_auto_20180210_1440','2018-02-10 15:35:28.790774'),(34,'apps','0006_auto_20180210_1441','2018-02-10 15:35:28.858014'),(35,'apps','0007_auto_20180210_1445','2018-02-10 15:35:28.916048'),(36,'apps','0008_auto_20180210_1445','2018-02-10 15:35:28.973525'),(37,'apps','0009_auto_20180210_1445','2018-02-10 15:35:29.115361'),(38,'apps','0010_auto_20180210_1445','2018-02-10 15:35:29.180905'),(39,'apps','0011_auto_20180210_1446','2018-02-10 15:35:29.281878'),(40,'apps','0012_auto_20180210_1446','2018-02-10 15:35:29.341530'),(41,'apps','0013_auto_20180210_1446','2018-02-10 15:35:29.399808'),(42,'apps','0014_auto_20180210_1447','2018-02-10 15:35:29.457805'),(43,'apps','0015_auto_20180210_1447','2018-02-10 15:35:29.525664'),(44,'apps','0016_auto_20180210_1448','2018-02-10 15:35:29.583447'),(45,'apps','0017_auto_20180210_1448','2018-02-10 15:35:29.642178'),(46,'apps','0018_auto_20180210_1450','2018-02-10 15:35:29.699611'),(47,'apps','0019_auto_20180210_1451','2018-02-10 15:35:29.757867'),(48,'apps','0020_auto_20180210_1452','2018-02-10 15:35:29.817468'),(49,'apps','0021_auto_20180210_1453','2018-02-10 15:35:29.876301'),(50,'apps','0022_auto_20180210_1454','2018-02-10 15:35:29.933189'),(51,'apps','0023_auto_20180210_1455','2018-02-10 15:35:29.992772'),(52,'apps','0024_auto_20180210_1456','2018-02-10 15:35:30.151285'),(53,'apps','0025_auto_20180210_1457','2018-02-10 15:35:30.208902'),(54,'apps','0026_auto_20180210_1458','2018-02-10 15:35:30.266114'),(55,'apps','0027_auto_20180210_1459','2018-02-10 15:35:30.333467'),(56,'apps','0028_auto_20180210_1500','2018-02-10 15:35:30.391478'),(57,'apps','0029_auto_20180210_1501','2018-02-10 15:35:30.450720'),(58,'apps','0030_auto_20180210_1502','2018-02-10 15:35:30.507981'),(59,'apps','0031_auto_20180210_1503','2018-02-10 15:35:30.566120'),(60,'apps','0032_auto_20180210_1504','2018-02-10 15:35:30.616609'),(61,'apps','0033_auto_20180210_1505','2018-02-10 15:35:30.674789'),(62,'apps','0034_auto_20180210_1506','2018-02-10 15:35:30.725112'),(63,'apps','0035_auto_20180210_1507','2018-02-10 15:35:30.775110'),(64,'apps','0036_auto_20180210_1508','2018-02-10 15:35:30.824648'),(65,'apps','0037_auto_20180210_1509','2018-02-10 15:35:30.866765'),(66,'apps','0038_auto_20180210_1510','2018-02-10 15:35:30.916563'),(67,'apps','0039_auto_20180210_1511','2018-02-10 15:35:30.967094'),(68,'apps','0040_auto_20180210_1512','2018-02-10 15:35:31.075531'),(69,'apps','0041_auto_20180210_1513','2018-02-10 15:35:31.125287'),(70,'apps','0042_auto_20180210_1514','2018-02-10 15:35:31.175634'),(71,'apps','0043_auto_20180210_1515','2018-02-10 15:35:31.225081'),(72,'apps','0044_auto_20180210_1516','2018-02-10 15:35:31.267263'),(73,'apps','0045_auto_20180210_1517','2018-02-10 15:35:31.317275'),(74,'apps','0046_auto_20180210_1518','2018-02-10 15:35:31.367489'),(75,'apps','0047_auto_20180210_1519','2018-02-10 15:35:31.417693'),(76,'apps','0048_auto_20180210_1520','2018-02-10 15:35:31.467842'),(77,'apps','0049_auto_20180210_1521','2018-02-10 15:35:31.517988'),(78,'apps','0050_auto_20180210_1523','2018-02-10 15:35:31.567590'),(79,'apps','0051_auto_20180210_1524','2018-02-10 15:35:31.617781'),(80,'apps','0052_auto_20180210_1525','2018-02-10 15:35:31.669338'),(81,'apps','0053_auto_20180210_1526','2018-02-10 15:35:31.729405'),(82,'apps','0054_auto_20180210_1527','2018-02-10 15:35:31.775962'),(83,'apps','0055_auto_20180210_1528','2018-02-10 15:35:31.826944'),(84,'apps','0056_auto_20180210_1529','2018-02-10 15:35:31.877700'),(85,'apps','0057_auto_20180210_1530','2018-02-10 15:35:31.926113'),(86,'apps','0058_auto_20180210_1531','2018-02-10 15:35:31.984368'),(87,'apps','0059_auto_20180210_1532','2018-02-10 15:35:32.052106'),(88,'apps','0060_auto_20180210_1533','2018-02-10 15:35:32.111474'),(89,'apps','0061_auto_20180210_1534','2018-02-10 15:35:32.161039'),(90,'apps','0062_auto_20180210_1535','2018-02-10 15:35:32.211291'),(91,'apps','0063_auto_20180210_1536','2018-02-10 15:36:26.495889'),(92,'apps','0064_auto_20180210_1537','2018-02-10 15:37:40.734399'),(93,'apps','0065_auto_20180210_1538','2018-02-10 15:38:42.901789'),(94,'apps','0066_auto_20180210_1539','2018-02-10 15:39:44.865558'),(95,'apps','0067_auto_20180210_1540','2018-02-10 15:40:46.856685'),(96,'apps','0068_auto_20180210_1541','2018-02-10 15:41:49.260372'),(97,'apps','0069_auto_20180210_1542','2018-02-10 15:42:51.568891'),(98,'apps','0070_auto_20180210_1543','2018-02-10 15:43:53.871743'),(99,'apps','0071_auto_20180210_1544','2018-02-10 15:44:56.406731'),(100,'apps','0072_auto_20180210_1545','2018-02-10 15:45:58.901977'),(101,'apps','0073_auto_20180210_1546','2018-02-10 15:47:01.342453'),(102,'apps','0074_auto_20180210_1547','2018-02-10 15:48:03.465595'),(103,'apps','0075_auto_20180210_1548','2018-02-10 15:49:05.646529'),(104,'apps','0076_auto_20180210_1549','2018-02-10 15:50:07.678368'),(105,'apps','0077_auto_20180210_1550','2018-02-10 15:51:10.176954'),(106,'apps','0078_auto_20180210_1551','2018-02-10 15:52:12.739702'),(107,'apps','0079_auto_20180210_1552','2018-02-10 15:53:15.197782'),(108,'apps','0080_auto_20180210_1554','2018-02-10 15:54:17.672209'),(109,'apps','0081_auto_20180210_1555','2018-02-10 15:55:20.149868'),(110,'apps','0082_auto_20180210_1556','2018-02-10 15:56:22.572951'),(111,'apps','0083_auto_20180210_1557','2018-02-10 15:57:25.001779'),(112,'apps','0084_auto_20180210_1558','2018-02-10 15:58:27.438964'),(113,'apps','0085_auto_20180210_1559','2018-02-10 15:59:29.480301'),(114,'apps','0086_auto_20180210_1600','2018-02-10 16:00:31.511860'),(115,'apps','0087_auto_20180210_1601','2018-02-10 16:01:33.775519'),(116,'apps','0088_auto_20180210_1602','2018-02-10 16:02:36.333121'),(117,'apps','0089_auto_20180210_1603','2018-02-10 16:03:38.311825'),(118,'apps','0090_auto_20180210_1604','2018-02-10 16:04:24.435421'),(119,'apps','0091_auto_20180210_1605','2018-02-10 16:05:26.975804'),(120,'apps','0092_auto_20180210_1606','2018-02-10 16:06:30.067496'),(121,'apps','0093_auto_20180210_1607','2018-02-10 16:07:32.181923'),(122,'apps','0094_auto_20180210_1608','2018-02-10 16:08:34.533333'),(123,'apps','0095_auto_20180210_1609','2018-02-10 16:09:41.775255'),(124,'apps','0096_auto_20180210_1611','2018-02-10 16:11:18.479789'),(125,'apps','0097_auto_20180210_1611','2018-02-10 16:12:06.077587'),(126,'apps','0097_auto_20180210_1611','2018-02-10 16:12:06.891997'),(127,'apps','0097_auto_20180210_1611','2018-02-10 16:12:08.212082'),(128,'apps','0098_auto_20180210_1612','2018-02-10 16:12:51.751855'),(129,'apps','0098_auto_20180210_1612','2018-02-10 16:12:55.645621'),(130,'apps','0002_auto_20180409_1305','2018-04-15 11:40:00.956131'),(131,'apps','0003_auto_20180411_1032','2018-04-15 11:40:01.016211'),(132,'apps','0004_auto_20180416_1613','2018-04-16 16:15:40.208530'),(133,'apps','0005_agencyuserlist','2018-04-16 18:51:14.126463'),(134,'apps','0006_auto_20180417_1843','2018-04-17 18:43:48.057859'),(135,'apps','0007_agency_agency_name','2018-04-18 18:08:21.494707'),(136,'apps','0008_agency_contact_no','2018-04-18 19:52:04.700493'),(137,'apps','0009_auto_20180421_1738','2018-04-21 17:43:42.575853'),(138,'apps','0010_agencyclient_creditdate','2018-04-21 17:43:42.633125'),(139,'apps','0004_auto_20180421_1227','2018-04-21 12:27:40.370977'),(140,'apps','0002_auto_20180421_1236','2018-04-21 12:36:25.834019'),(141,'apps','0011_auto_20180422_1541','2018-04-22 15:41:16.775426'),(142,'apps','0012_auto_20180422_1715','2018-04-22 17:15:40.097502'),(143,'apps','0013_auto_20180422_1723','2018-04-22 17:23:45.317703'),(144,'apps','0014_auto_20180422_1955','2018-04-22 19:55:41.986668'),(145,'apps','0015_auto_20180423_1523','2018-04-23 15:24:06.371903'),(146,'apps','0016_auto_20180423_1837','2018-04-23 18:38:12.368349'),(147,'apps','0017_twofactorauth','2018-04-26 15:36:54.644481'),(148,'otp_hotp','0001_initial','2018-04-26 15:36:55.853819'),(149,'otp_static','0001_initial','2018-04-26 15:36:58.349827'),(150,'otp_totp','0001_initial','2018-04-26 15:36:59.496472'),(151,'apps','0018_auto_20180426_1540','2018-04-26 15:40:19.195739'),(152,'apps','0019_facebookpagedetails','2018-05-03 14:16:32.444180'),(153,'otp_yubikey','0001_initial','2018-05-03 14:16:36.898819'),(154,'two_factor','0001_initial','2018-05-03 14:16:38.026749'),(155,'two_factor','0002_auto_20150110_0810','2018-05-03 14:16:38.155972'),(156,'two_factor','0003_auto_20150817_1733','2018-05-03 14:16:38.354728'),(157,'two_factor','0004_auto_20160205_1827','2018-05-03 14:16:39.320273'),(158,'two_factor','0005_auto_20160224_0450','2018-05-03 14:16:40.364850'),(159,'apps','0020_facebookpagedetails_page_id','2018-05-03 13:22:15.470104'),(160,'apps','0021_auto_20180503_1938','2018-05-03 13:38:24.799573'),(161,'apps','0022_campaignanalysis_analysis_time','2018-05-07 10:43:53.640008');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('02osrb35sdtym4f5olx3np9mp2z82l2l','ZGIzYzAyMzJmOWM2MzY3NjU4NDdhMDc4M2E1NTM3NTk5ZTA4YWRiNzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2017-12-31 12:22:25.295548'),('03bnvn073r0meapy5d8ki54jo1swj8yu','MTc2ZGE4NjNkM2E0N2FiNDMxYTVmYTJjZjFkYjRmNWFhM2NlMTZjNTp7Il9hdXRoX3VzZXJfaGFzaCI6ImZlYmVjOTM2N2QzYmNiMzEyYjgyZTllYmFkMzZlN2RjNjA2YmM0ZDUiLCJfYXV0aF91c2VyX2lkIjoiNDIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9','2018-05-24 10:24:35.350540'),('08vgrrm6ob6476wj8lsz7rqh9n6tzi5r','ZWI3ZDRhODJiNWFlZTFmMTFkNTQzZTM3M2FiMzQzYjhmNDlhMWM2ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-30 13:51:08.073105'),('0aww8n357r3tez8tk1gfiqvc7v28x0c5','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-08 13:45:21.030311'),('0br32erb2gw5azil8pp3rhxhnebgv1di','YWRkNWVlNGFlOWM5MWExZjg4MjdmY2M5ODI2MGYyZGY1NWJiMTUyODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-01-20 18:55:11.178418'),('0cgdjf3nbitfaeb9k25fgz4756so0x8c','ZjYzZWQ1NzMzMWZlNTNlOGM1YWRiNTAzYmEzYzlmYTRhY2UxMWNhZTp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-11 14:34:56.819025'),('0ij3eq2amhgqibkoschhq2aj9b2lrdot','MTdhYzViNDhmOGViMTg2ZjFlMzQ3MjcwY2JjZmQ3NDc0ZjRkNzU4OTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfc2Vzc2lvbl9leHBpcnkiOjAsIl9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYifQ==','2018-01-21 14:31:50.274333'),('0pmncshk001x05842qf3dr58x5ewdxn5','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-19 13:38:46.649839'),('0rfjlxtwfk3zyu64yreq6w9peaaomi2w','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-03-03 14:36:54.472937'),('0sd6ed8eb4vw6xkuzhnz3kooleplew38','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-03-26 05:47:39.977528'),('167u2eqk6tw8oyay0u5qdduok0b318uw','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-03 15:34:53.143471'),('16skt17hobsyxbq4fw8rw5kj8624k9m1','NzdlMDMyMjEwNjA2M2NhYjkxMjRhNzc1ZDA3MjI0M2NhNjM3N2EyNzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-05-17 06:32:23.436876'),('1fe6yqgim0lbw5vztg8xe3expn7531r3','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-04-15 14:29:17.330710'),('1geyk0ryg0tu4gv4nuqqjeljoq48yo3u','ZWI3ZDRhODJiNWFlZTFmMTFkNTQzZTM3M2FiMzQzYjhmNDlhMWM2ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-14 10:01:11.084148'),('1gjhmp9zf2xga9xcck7mq5wx68edevu5','ZWI3ZDRhODJiNWFlZTFmMTFkNTQzZTM3M2FiMzQzYjhmNDlhMWM2ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-03-20 13:41:59.368766'),('1qay9ugarmrx3zbli68ub3v4mfbkutf5','YWRkNWVlNGFlOWM5MWExZjg4MjdmY2M5ODI2MGYyZGY1NWJiMTUyODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-01-20 03:16:38.005480'),('1rzsa9vms03rq1d36gokolaq4k0ty88j','YWRiYjdmMGQxYWRlN2JhMDNlNjU3OWQ2MjA0MTZmY2QzMmY1MWQwODp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-04-18 12:31:40.349353'),('1xi31k802t4rximbsp307wzw0p86o22d','ZWI3ZDRhODJiNWFlZTFmMTFkNTQzZTM3M2FiMzQzYjhmNDlhMWM2ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-05-24 15:50:24.724335'),('1yku8263k6gtdet94n7ulhn60wt1b624','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-04-14 15:15:36.667624'),('2c8gmbpf2mrqznsoynuykt4ujvpmg01k','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-03-03 14:11:19.252324'),('2diil8gs4f4qult3uto6idde2kwpl3sz','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-24 01:10:23.485651'),('2dj1vi0q9s7m8cijzvg6xwn0jk2tizae','YWRiYjdmMGQxYWRlN2JhMDNlNjU3OWQ2MjA0MTZmY2QzMmY1MWQwODp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-13 11:30:58.022573'),('2ry1zzq6etsh7stpjfixing1gws3b4d5','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-04-26 09:57:52.265951'),('2tdl4gxshx7l4qtf72lu4y3i7o2n6pvs','OTgyYmY2YzlkY2ViY2NlMTY2YWM4OGYwODc4OWRiMzc1Y2E5NTUyYzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-08 13:42:20.807607'),('2tkjwytdqe5u17jc4w0v1y8n96j787zb','MTA5ZWY2OWMxZTFhZWI4NDg0NDY3MjkyNzUyN2M4M2FiYzNmMWE4NTp7IndpemFyZF9jb250YWN0X3dpemFyZCI6eyJzdGVwIjoiMSIsInN0ZXBfZGF0YSI6eyIwIjp7IjAtc2VuZGVyIjpbInNpLm5heW9uQGdtYWlsLmNvbSJdLCIwLXN1YmplY3QiOlsidGVzdCBvbmUiXSwiY3NyZm1pZGRsZXdhcmV0b2tlbiI6WyJ3T3ZUYlkwc0I2NnVBelQzV0pEa21rSmFOemF1dUEzbFhmbmt4TEJDZXNrVTVPbFFlNXhBNk13d2xvVXRkWktYIl0sImNvbnRhY3Rfd2l6YXJkLWN1cnJlbnRfc3RlcCI6WyIwIl0sInN1Ym1pdCI6WyJzdWJtaXQiXX19LCJzdGVwX2ZpbGVzIjp7IjAiOnt9fSwiZXh0cmFfZGF0YSI6e319fQ==','2018-05-13 15:55:51.515843'),('2w5krlesghckdc8ybvn8q9771sk2j1ng','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-21 09:43:07.502010'),('33ddt25vv8r0gg269zy2350gkcqwfrvn','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-04-24 11:40:58.560059'),('35wndll7xrjqi6ij0db3ms8bse00u4jp','ODUxZmRjMzcyNmUzOTUwZGZhNmNmZjQ4OWRiMDgwMDc5Mzc1OTc0YTp7IndpemFyZF9jb250YWN0X3dpemFyZCI6eyJleHRyYV9kYXRhIjp7fSwic3RlcF9kYXRhIjp7IjAiOnsiMC1Vc2VybmFtZSI6WyJhZG1pbiJdLCJjb250YWN0X3dpemFyZC1jdXJyZW50X3N0ZXAiOlsiMCJdLCJjc3JmbWlkZGxld2FyZXRva2VuIjpbIkxla0FvdDhIamFIQmZEUDByWkdHdU5OZ0IySTJnbGtiNE54cFczZDZoRWQ4eHU4ZlY5bmJ6VzRkZDlyVDF3YnQiXSwic3VibWl0IjpbInN1Ym1pdCJdLCIwLVBhc3N3b3JkIjpbInBhc3MxMjM0Il19LCIxIjp7ImNzcmZtaWRkbGV3YXJldG9rZW4iOlsidExtZ2wxOUtFeGpGT2htdXJYdEhSdVhNQkgydkNwdmNNa3o1VEJlOUMxUGM2OEZKVjdhY1dEZUpkT0xtbkFtdSJdLCJjb250YWN0X3dpemFyZC1jdXJyZW50X3N0ZXAiOlsiMSJdLCIxLXZlcmlmaWNhdGlvbiI6WyI2NjE4OCJdLCJzdWJtaXQiOlsic3VibWl0Il19fSwic3RlcCI6IjEiLCJzdGVwX2ZpbGVzIjp7IjAiOnt9LCIxIjp7fX19fQ==','2018-05-13 20:42:03.806073'),('36zbz7oo8cmfd6t6cqo1y4nxjcuhyiu8','MWE5YTQ0MmM4ZTk2NGYxOTQ0MzUwYjYyNjFmNjQ3M2M0ZjQ4ODI0YTp7Il9hdXRoX3VzZXJfaGFzaCI6ImYxMTUxNDViZjk4Y2NiNmYxMDQxYzkwMzljZDZmNTQ2MDBiYmVmZWEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI0MyJ9','2018-05-22 10:57:46.974062'),('38bx95wzx1lgxbppofr5o99zvu34xgjl','ZGIzYzAyMzJmOWM2MzY3NjU4NDdhMDc4M2E1NTM3NTk5ZTA4YWRiNzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-13 22:48:51.027401'),('3d3ii6whk3kf1rjrjzusr1jqf7s4vf1e','OWFkOTcyZjQzZjI2OWYxMGQ3ZWYzZjQ5Mjg3ODE4NzVjZjdjZTgwNjp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4YjFiYzRiY2I1MzdjZDFlNzlkNjdmODE5OTc0NWNkYTk0MDEwM2RhIiwiX2F1dGhfdXNlcl9pZCI6IjQ4In0=','2018-05-17 13:23:10.809810'),('3eysgeuogdzhenatmfkg2li3uzeof4l7','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-03 12:18:43.655495'),('3j2u487skqsia7tt4xewog1c4zdyy8o0','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-12 14:45:09.829902'),('3ju001ylbz3i9b8jkjoymaj28m24tggr','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-05-23 12:28:04.205923'),('3jyui6he86jgz6i94ic3hvk0sa6wuk5k','OTM0Zjc4Yjc0ZmIwMGMyODc5NTcyY2E5NjI1ZTBlNzg2ODMyYzlmYTp7Il9hdXRoX3VzZXJfaGFzaCI6Ijk3YjdkMDQ0YTc3YTZjNGMzMGJiNjQwOTlhZmNkMmU1OTdlYzQyN2YiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIzMCJ9','2018-05-06 19:32:34.001614'),('3kg7guggjgazw9ya41dzxadrx355h0g8','ZjU5NTBlM2VkZmMyZGViNzNhYjRkZTJmODFjZmNhYWU2MTk1ZGJiODp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYifQ==','2018-05-21 13:02:32.782879'),('3m0nj7vq9rfnsmegg4qortutnxnfdbu0','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-15 07:52:16.354049'),('3ne2cwhs0yjxd8u1krwi89r83s4ixdzz','ZWI3ZDRhODJiNWFlZTFmMTFkNTQzZTM3M2FiMzQzYjhmNDlhMWM2ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-11 12:11:20.164998'),('3ny6qst9r4hxmew2zss95jtg10gbn1kz','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-21 05:19:13.351662'),('3r6tw8vsnavr5t3zsyy52ws4mpzaagx3','YWRkNWVlNGFlOWM5MWExZjg4MjdmY2M5ODI2MGYyZGY1NWJiMTUyODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-01-17 16:28:01.392734'),('3vkbk724qkf29ss0tlrmgz62tj3rmco1','ZjgyYTgzMTU5ZWZhNTc1ZjE5YThjNTE3ZTA1NzdjMjFhOTQzZGFmNjp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiMzFiYWFmYWY1ZTUxNDBkNTljOWE3ZTgzM2IzYzBiM2VkODg2NzcyZCIsIl9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-08 00:25:53.949465'),('3weuvzlmaamlmf9l6xasl1cmkh1awko3','ZDcwODE3MzA5ZThlYWRhODk3MDFlZmVhMzQyNTg2ZDdlZGVmZjVmMTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-12-29 08:12:46.763442'),('3x1ek09j3cpxsg5cc0uat5bnwkbmqo7m','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-23 23:29:16.902867'),('4045m2o66290zbljjlqpfo1a0zrehmf7','ZjhmMzRjMWMyYmQ0OTc2OTU4ZjQ3OTZkYjZlNDNmNzQzZWQ4N2RhNTp7IndpemFyZF9sb2dpbl93aXphcmQiOnsic3RlcF9kYXRhIjp7fSwic3RlcCI6IjAiLCJleHRyYV9kYXRhIjp7fSwic3RlcF9maWxlcyI6e319fQ==','2018-05-14 14:11:31.071624'),('40rjiq81er05lr55jpm7zkofbv92uhas','ZjYzZWQ1NzMzMWZlNTNlOGM1YWRiNTAzYmEzYzlmYTRhY2UxMWNhZTp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-08 17:57:35.555271'),('44965749rizc0egz3uoyjpqlxjsdyk1z','ZjgyYTgzMTU5ZWZhNTc1ZjE5YThjNTE3ZTA1NzdjMjFhOTQzZGFmNjp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiMzFiYWFmYWY1ZTUxNDBkNTljOWE3ZTgzM2IzYzBiM2VkODg2NzcyZCIsIl9hdXRoX3VzZXJfaWQiOiI0IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-08 00:26:11.309751'),('47kbcpay458ewabtkja6mi4jqygeae05','ZWI3ZDRhODJiNWFlZTFmMTFkNTQzZTM3M2FiMzQzYjhmNDlhMWM2ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-10 13:25:08.514857'),('4cjb4i5k6jp4pyyjvqs8rdhplbdvsg12','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-13 09:31:55.799566'),('4ctvn3zm7idkpz26virk6r4r782gf030','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-04-14 12:59:33.717816'),('4e8roklbx5t659dcmd1vvjzmdm3atc7c','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-17 13:29:23.884777'),('4esrzew9i6zlnht869yeicnfgcddigfy','ZjAwODk3OWExYWQ0OTUyYTdiYTQ4N2Q5ZTgwNjYzYzVjOTExOGI3Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-05-16 11:39:59.396720'),('4g50n1l2a9c56dosdgns5aipqt830inq','NzdlMDMyMjEwNjA2M2NhYjkxMjRhNzc1ZDA3MjI0M2NhNjM3N2EyNzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-05-20 05:52:13.309631'),('4ipfgbhdg4rgucwxdk0zk4idjmwafutn','ZGIzYzAyMzJmOWM2MzY3NjU4NDdhMDc4M2E1NTM3NTk5ZTA4YWRiNzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2017-12-31 11:44:15.757522'),('4lknng9a0fh1ve5qjucbyyvtc03y8vru','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-05-23 17:56:20.525004'),('4pnxps05q3pwxiklbop0n274cdnsiw01','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-03-10 13:25:39.593810'),('4pv1cpf5xixcetitaodgcg3ww5vkbik6','ZDcwODE3MzA5ZThlYWRhODk3MDFlZmVhMzQyNTg2ZDdlZGVmZjVmMTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-12-29 04:38:06.247718'),('4q99t2mxhm515m9s6jpyxmmlld0psbka','NDQxYTczNTE0NDI4MmNmMWM2MmQ3MThjZTQzOTQ1ZmUzMGVlODU3Yzp7Il9hdXRoX3VzZXJfaWQiOiI0OCIsIl9hdXRoX3VzZXJfaGFzaCI6ImYwYzYyNWIwMWE4NWQ3N2I1OWQ1ODY2MTYzMTVhNTc3ZjliYjgwNDEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9','2018-05-23 19:47:31.303989'),('53hlictsuyd3z2ponhfieb4antc0yzca','YjZjM2IyZjlkOTI4NTJlODZiMTRmNjljYzQ4YzYyMGEwYWVhOTg2Zjp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-02 07:11:17.072273'),('547ltvqzfqr5rw98h9hmwrtg7h8swqep','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-03-03 10:10:46.799915'),('54srs5wyifco69xx72cgse3kvubt5cqe','ZjhmMzRjMWMyYmQ0OTc2OTU4ZjQ3OTZkYjZlNDNmNzQzZWQ4N2RhNTp7IndpemFyZF9sb2dpbl93aXphcmQiOnsic3RlcF9kYXRhIjp7fSwic3RlcCI6IjAiLCJleHRyYV9kYXRhIjp7fSwic3RlcF9maWxlcyI6e319fQ==','2018-05-14 14:03:55.122051'),('57bdsot4kmmrqsz61pzo04i4uc0anaqq','NzdlMDMyMjEwNjA2M2NhYjkxMjRhNzc1ZDA3MjI0M2NhNjM3N2EyNzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-05-15 18:47:22.663656'),('587oxbxxm2g8vkmv0pnb50mk3hohuppf','YTkwZDhiOWZiM2I0MmVkNjcxMGJlODdiY2QwZTAzZjcxMTE0YjA0MTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-01-22 19:43:04.689722'),('5bobmxbiszeq3in768ngvk3hitkomlav','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-05 12:51:19.686324'),('5ch6gd8vnto148h8m5e0kfozhzdfpc6r','ZDcwODE3MzA5ZThlYWRhODk3MDFlZmVhMzQyNTg2ZDdlZGVmZjVmMTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-12-29 14:35:13.369969'),('5gy1qe79tkvqtrbl7i58vq6m45tf4f1x','NjE4NjNiNzNkNWFiY2NhYmU1NDVkM2NiZDNmYzliYjNlY2ViMzM5Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-05-20 08:00:23.319055'),('5hcx4ewo0763d2qauhhuuku2i5978lhn','ZGIzYzAyMzJmOWM2MzY3NjU4NDdhMDc4M2E1NTM3NTk5ZTA4YWRiNzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2017-12-31 10:51:19.473641'),('5krlh7vwv1i94g1arxu5slwtixcpl9u0','ZjAwODk3OWExYWQ0OTUyYTdiYTQ4N2Q5ZTgwNjYzYzVjOTExOGI3Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-04 06:34:30.301227'),('5pvrvdmu620h4eifdmqirou1ycds795h','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-02-28 14:05:53.233218'),('5st7u97oi3omkxr52m6wdhwuwwmed8qj','YTkwZDhiOWZiM2I0MmVkNjcxMGJlODdiY2QwZTAzZjcxMTE0YjA0MTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-05-21 13:27:21.460127'),('5t5lzixzygvrpnof67nalraeuuftihbl','YjZjM2IyZjlkOTI4NTJlODZiMTRmNjljYzQ4YzYyMGEwYWVhOTg2Zjp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-03 05:56:56.075226'),('5tayiocn921jr604tr0r6dhg9qh4mpaf','YTllOThjZGYwNGFlNjAyNmQ0ODMxZWIyZjdmOGViNTg4NDg1ODA5ODp7IndpemFyZF9sb2dpbl93aXphcmQiOnsic3RlcCI6IjAiLCJzdGVwX2ZpbGVzIjp7fSwiZXh0cmFfZGF0YSI6e30sInN0ZXBfZGF0YSI6e319LCJ3aXphcmRfY29udGFjdF93aXphcmQiOnsic3RlcCI6IjAiLCJzdGVwX2ZpbGVzIjp7fSwiZXh0cmFfZGF0YSI6e30sInN0ZXBfZGF0YSI6e319fQ==','2018-05-14 13:23:28.272986'),('62gj8hd0g1vsmywv8stz7fub2p6s9pki','OTgyYmY2YzlkY2ViY2NlMTY2YWM4OGYwODc4OWRiMzc1Y2E5NTUyYzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-12-30 09:42:23.369633'),('676pv0b5x70chzura0uigir2avy1c0q8','MmQzYTExNmM4OTgyMDhhMjBmZmMwMDc5NzkyMWMyNzE0ZTYwZjljODp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYifQ==','2018-01-14 08:14:06.636176'),('6b51y3437iifqwlv4shdbc3yncomi6ul','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-03-10 14:50:15.021436'),('6c9b9idujlcrjijpsuslr9d2swxw4ds3','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-13 13:05:20.919350'),('6ds0zw4grubiikpq9idswqz6pzo7sj69','YWRiYjdmMGQxYWRlN2JhMDNlNjU3OWQ2MjA0MTZmY2QzMmY1MWQwODp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-05-01 09:04:26.600904'),('6et3th4s3qc46hxahaep7r2u5lksx3ug','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-21 09:14:34.099767'),('6n0exw11m9vjy3p0ewznzlnxudz7r50g','MDFkMGNmZTRiYTU2NGM2NmJlMWE5MTZhMGY5YjBmZGVkN2EzYjMwMDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-04-17 06:59:45.189586'),('6puu8996hzq72a2h4ipsixsanfoxxtfw','ZjAwODk3OWExYWQ0OTUyYTdiYTQ4N2Q5ZTgwNjYzYzVjOTExOGI3Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-03-03 20:50:45.440889'),('6tpf48w7usuh92d9w1lr3kxj7u4mdblp','NzFkMGY4MjUxMzE1NTI1ZTZlZWY5Mjc1NGE0NjdmOGIxZTA5M2Q4Yzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2017-12-31 10:38:55.992979'),('6tt74150500j39tfozfh2ri7wpzw0iv2','ZWI3ZDRhODJiNWFlZTFmMTFkNTQzZTM3M2FiMzQzYjhmNDlhMWM2ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-11 13:13:01.380821'),('6xcsucswpl62lypu2xtuo5gxxfr6m2fi','ZDIxODBhNzEzZWE3MzYyNDgyNGU1NzllMTI2ZWMyZGQzMGQ0NzA0Zjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYifQ==','2018-05-20 10:39:04.740511'),('70vf0qxyprml414refvnjywuct5lak1c','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-09 13:59:17.189750'),('76z11i97mlzzn2ojqj0mv82xbcv2z98a','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-10 12:25:45.664072'),('772j8yxi8w2vw91hij8n2r874trqvugv','OTA4YTZmMDRlYWFkZmUwY2JhNzQxMDdmMDAzMDk0ZWVlZjJhYjhjYTp7Il9hdXRoX3VzZXJfaWQiOiI0MiIsIl9hdXRoX3VzZXJfaGFzaCI6ImZlYmVjOTM2N2QzYmNiMzEyYjgyZTllYmFkMzZlN2RjNjA2YmM0ZDUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9','2018-05-23 12:40:32.879608'),('775z2h1z19swy57vqqu4yndk42m9cs7r','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-18 06:16:11.482846'),('7dzi2acemojdn744t9ftpuplzpsx11c3','ZjYzZWQ1NzMzMWZlNTNlOGM1YWRiNTAzYmEzYzlmYTRhY2UxMWNhZTp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-08 17:18:23.535574'),('7gg7hqhy8p2z4ti4zsjwvkbi0vzlh1qa','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-04-29 12:45:04.548161'),('7j0qz1kngkj3idk5qa8d27pp9ath1hkp','NzE3NjViNDcyMWFhNGNiNzA0NjRjODU1ZmNhM2I1N2EzMDRjNTU2OTp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiNGIwMjY5YTAxMGUzOTI2YmY5MjUxZWJhOTkzYjIxNGVjOWQyZmRiMCIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-08 00:52:25.601308'),('7muy6bpc48mcvsqh6dzkoyjtgji5gobg','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-05-03 07:23:29.277297'),('7pxfr3h0clv2pebkh81jegb3agn7by4d','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-04-02 12:06:44.565961'),('7q36w4assb4ft1ffy36n2o9ij167tim8','NDMzMWY4YmQ0OGQwYTAwMTM2OTgzZTAzNmU1OTI4ZmIzZDFhY2I2Mjp7Il9hdXRoX3VzZXJfaWQiOiIyOCIsIl9hdXRoX3VzZXJfaGFzaCI6ImY5NjkxODA5M2U2MmMxYmFlZGI2MWExMTg4M2NlODZlNTk4ZGFiYzAiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9','2018-05-02 10:37:09.856942'),('7s0b8wc5ocwstvw2544hqsikzxyxi8oy','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-04 11:54:53.210856'),('7u9o54zw4ukdsdljod8watsbkmv92l2r','NDMzMWY4YmQ0OGQwYTAwMTM2OTgzZTAzNmU1OTI4ZmIzZDFhY2I2Mjp7Il9hdXRoX3VzZXJfaWQiOiIyOCIsIl9hdXRoX3VzZXJfaGFzaCI6ImY5NjkxODA5M2U2MmMxYmFlZGI2MWExMTg4M2NlODZlNTk4ZGFiYzAiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9','2018-05-07 20:21:32.194358'),('7vss4lnh76d4wj9th78en9p2daoxzmsa','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-04-02 05:08:28.339379'),('7xrpol3zup86byr4w7013s71rnnq17ie','ZDVlMmQwY2JjMmI3MDVhNDg3NWRkM2UzNzEyNmQwYjBiYmUxODNmNjp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfc2Vzc2lvbl9leHBpcnkiOjAsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-04 18:57:51.841365'),('83e0dims7df68y1x1znuaptfxnsqiz54','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-04-12 13:02:04.607025'),('8byhuq0tgrz5rbd23rj1og8p2jr91crq','OTgyYmY2YzlkY2ViY2NlMTY2YWM4OGYwODc4OWRiMzc1Y2E5NTUyYzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-07 18:12:49.714253'),('8f33aw5qkzsyv85c59b8er3s0y5hnpav','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-13 13:06:58.475177'),('8gxuz1k1jzk1bu2yek0civorlpzgzslt','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-04-08 13:17:45.647359'),('8mgq1cw76aubiqmml407zeghddidxao9','N2MzODVkNDk5NmQyNDE4M2I4NGMwNjFiZmMwNjU1YTJkMGU4MDNjYTp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-03-26 13:23:53.088633'),('8oklsml91dw57llhofzbdi2hzzglai2p','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-02-28 13:39:17.727458'),('8qngpj8uhi6wnoa0zxzr4l25tsciuana','ZDIxODBhNzEzZWE3MzYyNDgyNGU1NzllMTI2ZWMyZGQzMGQ0NzA0Zjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYifQ==','2018-04-07 14:55:31.381083'),('8r9s8fsar0ip3xflakk2mg9sfsainyh1','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-03 06:18:05.780596'),('8rkex4kon0z8h4gy8mp4a5yqbus361ij','NDg5Mjc1YTJjYzI1YTM5YmJjZmVhNWIxYzNlNTMyNmQ5Yjc1NzFlNTp7Il9hdXRoX3VzZXJfaWQiOiIzMCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOTdiN2QwNDRhNzdhNmM0YzMwYmI2NDA5OWFmY2QyZTU5N2VjNDI3ZiJ9','2018-05-05 19:28:53.994351'),('8vrn0a2girokmpezenht9n1b7zi6axlz','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-04-05 07:31:30.481414'),('8x70tuuzq608gfodqn6obsgclral3jme','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-23 20:19:38.950358'),('909x3oyyisxavogunl15ancw1novgn7a','NjE4NjNiNzNkNWFiY2NhYmU1NDVkM2NiZDNmYzliYjNlY2ViMzM5Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-04-02 07:32:24.316048'),('90rdw3dpso98n3dvodod0patihhgbekp','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-04 11:57:03.565802'),('90uewhs8jozwucn4a768swbaezq6qqf7','OTgyYmY2YzlkY2ViY2NlMTY2YWM4OGYwODc4OWRiMzc1Y2E5NTUyYzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-08 11:56:34.254254'),('933fpimwljhjg4c2c07vg4ywv3ffx53w','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-01 20:18:14.862170'),('93c025nmger26pxe6ub3oqzui3ly7ff7','ZDcwODE3MzA5ZThlYWRhODk3MDFlZmVhMzQyNTg2ZDdlZGVmZjVmMTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-12-28 18:21:16.248659'),('95ab9x6oyp9s5kx2tf6glsvibmkmsrv2','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-04-24 12:00:49.257477'),('9ahdv0zfr0ppenmujtu3j53f19e5hkqa','ZDVlMmQwY2JjMmI3MDVhNDg3NWRkM2UzNzEyNmQwYjBiYmUxODNmNjp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfc2Vzc2lvbl9leHBpcnkiOjAsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-03 14:38:21.482721'),('9dj032wacwg0tqwthc8f3ns6nv47nwgs','YTkwZDhiOWZiM2I0MmVkNjcxMGJlODdiY2QwZTAzZjcxMTE0YjA0MTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2017-12-30 07:26:46.951891'),('9fd1fwf1s9ih7q6vfjkyrz2m7smulptg','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-22 12:41:05.858703'),('9k7784iobnsqvweviwmxa4a0ixxdno0m','OTgyYmY2YzlkY2ViY2NlMTY2YWM4OGYwODc4OWRiMzc1Y2E5NTUyYzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-08 13:46:12.909861'),('9m3tkfpbzqnycfnicdgf4ht7ywu4km5t','ZDcwODE3MzA5ZThlYWRhODk3MDFlZmVhMzQyNTg2ZDdlZGVmZjVmMTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-12-30 05:27:14.317765'),('9n3p0lgfx5n8m9omexpom02sj7jxe4ar','Y2M0ZDk5NzdjNWFkZGZiZDNmMDU1YWM1M2UyMDdlOTg5ZjMxNWE2Njp7Il9hdXRoX3VzZXJfaGFzaCI6IjgzYmEyYzQyZTQzYmZhZjY4NGMxZjMzYTkwMTQwNDQwOTliZmViYTMiLCJfc2Vzc2lvbl9leHBpcnkiOjAsIl9hdXRoX3VzZXJfaWQiOiI0NSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-05-20 07:24:11.336002'),('9pmpcbj2zkpaueixek0lpna6lk00d4fv','MTdhYzViNDhmOGViMTg2ZjFlMzQ3MjcwY2JjZmQ3NDc0ZjRkNzU4OTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfc2Vzc2lvbl9leHBpcnkiOjAsIl9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYifQ==','2018-05-22 10:38:19.041127'),('a0u4rkl4wjc6gqg3bedq9y3lkxajbnhe','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-04-25 09:38:00.562409'),('a2p7yxhv7u0vedahriyu9uyhp1441ja7','YWRiYjdmMGQxYWRlN2JhMDNlNjU3OWQ2MjA0MTZmY2QzMmY1MWQwODp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-03 07:53:47.083942'),('a63ik4n5in3z6br6z28hwvdvs3biigaj','OTgyYmY2YzlkY2ViY2NlMTY2YWM4OGYwODc4OWRiMzc1Y2E5NTUyYzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-07 20:43:11.277019'),('a7ctevj7ts2go66c2wzu5zfbxobhkreq','ZWI3ZDRhODJiNWFlZTFmMTFkNTQzZTM3M2FiMzQzYjhmNDlhMWM2ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-11 06:14:03.429400'),('adidbf43gh420zcbvhd1ak6o2tdhdfwh','ZmY0ZTQyNmM1Yzk5YmQ2N2RmYmNhMDkwYmY0NTg4NzYwNDk4OGZmYTp7Il9hdXRoX3VzZXJfaWQiOiIxMiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMjEzMGE3MDlkMzU5YTg5MjJkODVkOTBkY2Q4YzlmN2QyYWM5MDllYSJ9','2018-04-23 11:05:02.339032'),('ai2qy4w4zq81jfc7un9s7fr9nizi1j16','NjE4NjNiNzNkNWFiY2NhYmU1NDVkM2NiZDNmYzliYjNlY2ViMzM5Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-01-01 07:02:28.078894'),('ajw0ypn4rtv5mqhppunae56dqr1nwdvp','Y2VkNmM4NDhiODk0Y2Y0ZTg5YTA2OTJjODA3MmY2Y2Y2OTE2MDJmMTp7Im51bV92aXNpdHMiOjksIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-05-09 20:15:47.431630'),('ampycnszjc1nvkg5dgzo6ub22s52y7qo','NzFkMGY4MjUxMzE1NTI1ZTZlZWY5Mjc1NGE0NjdmOGIxZTA5M2Q4Yzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-05-22 10:28:41.359334'),('arzmnqmpem7golicj1fv6q30kb8xwj9y','OTgyYmY2YzlkY2ViY2NlMTY2YWM4OGYwODc4OWRiMzc1Y2E5NTUyYzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-12-31 05:32:44.673254'),('axaufyjvinflgamo3852f9iedt0fdj8e','YWRiYjdmMGQxYWRlN2JhMDNlNjU3OWQ2MjA0MTZmY2QzMmY1MWQwODp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-27 05:28:49.010940'),('b0ylntb4at2d8mtzf9866c2qyvua4s3e','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-29 09:37:00.701421'),('b2xah0d9lj90l02tj8eihlhuqo214m2f','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-04 01:00:01.566029'),('b6alg2kg7skorgilpvjp16291cq2u87t','YWRkNWVlNGFlOWM5MWExZjg4MjdmY2M5ODI2MGYyZGY1NWJiMTUyODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-05-21 13:51:20.713536'),('b6jm1761bm40v2ht1c2rxqz950l0kw4e','ZjAwODk3OWExYWQ0OTUyYTdiYTQ4N2Q5ZTgwNjYzYzVjOTExOGI3Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-05-16 09:39:28.755806'),('b7jr5q8zj4x1nf8htosh2ojk017o9m95','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-18 13:41:06.007674'),('b8d0ew19s120kqeyu9eew3hn3tvlgwb5','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-03 05:37:13.432363'),('b8ikebxgfw85epxz3s3qsxxoe0bzn4xu','YjZjM2IyZjlkOTI4NTJlODZiMTRmNjljYzQ4YzYyMGEwYWVhOTg2Zjp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-02 07:42:42.694832'),('b9rlkcujk6gwfugvbf4p3ptgs13qhv2x','OWFkOTcyZjQzZjI2OWYxMGQ3ZWYzZjQ5Mjg3ODE4NzVjZjdjZTgwNjp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI4YjFiYzRiY2I1MzdjZDFlNzlkNjdmODE5OTc0NWNkYTk0MDEwM2RhIiwiX2F1dGhfdXNlcl9pZCI6IjQ4In0=','2018-05-17 12:42:49.698088'),('bf2pbnoltjyxhim6p1mvkdevy66tkozn','ZDVlMmQwY2JjMmI3MDVhNDg3NWRkM2UzNzEyNmQwYjBiYmUxODNmNjp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfc2Vzc2lvbl9leHBpcnkiOjAsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-06 12:32:30.166832'),('bhao2lbku9thucy62xu2ewytuatgz2in','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-03-03 11:51:41.703188'),('bl42vzfhcqq3odbzn6qlh4pt5sy34pxd','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-10 14:26:21.686715'),('blxja1zkjzo1zd3d81y14lmsobpvqqv7','ZWI3ZDRhODJiNWFlZTFmMTFkNTQzZTM3M2FiMzQzYjhmNDlhMWM2ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-30 19:46:18.243731'),('bnyl5me8lj5vvrmuw9ijgexh675uvwhv','N2QzYjk0NDZjMDYyMGRhNzU1NGQwNjJjNTJhNWM0MjkxOGJlNGE1Yjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQ4IiwiX2F1dGhfdXNlcl9oYXNoIjoiZjBjNjI1YjAxYTg1ZDc3YjU5ZDU4NjYxNjMxNWE1NzdmOWJiODA0MSJ9','2018-05-23 20:40:59.478005'),('bpryeqo6t7ptznz7zp3c6ph5yin9e9s4','ZGIzYzAyMzJmOWM2MzY3NjU4NDdhMDc4M2E1NTM3NTk5ZTA4YWRiNzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-14 01:49:47.429151'),('bqndt3xrhb082vh5ftkno9l68nno4atz','ZDcwODE3MzA5ZThlYWRhODk3MDFlZmVhMzQyNTg2ZDdlZGVmZjVmMTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-12-30 04:19:27.062003'),('bumzmji58abck22yux5owsbfd8umgiej','ZWI3ZDRhODJiNWFlZTFmMTFkNTQzZTM3M2FiMzQzYjhmNDlhMWM2ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-25 11:59:18.466430'),('c146ztecj1v382uefi5e4jc3ik4m5n8a','ZDIxODBhNzEzZWE3MzYyNDgyNGU1NzllMTI2ZWMyZGQzMGQ0NzA0Zjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYifQ==','2018-05-20 11:43:33.153251'),('c2yaspbmjmscg6zaqz11snzu2iwg4ssf','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-02-24 09:41:28.725993'),('ccz8zkx2eudne3uor6a3pl3y6guu1zip','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-01 22:27:10.327875'),('cdfw0yunw7cgm0bgkz3n8d7df3zdodkg','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-03 06:03:13.712613'),('cel2m0wf4e8r47ny79d41pyddmiyt1q9','YzBhYzYyZTNiNDE3ZDMzYzc0NGMxYjExNDJkMTg4Y2MzODU3MDkyMzp7Il9hdXRoX3VzZXJfaGFzaCI6Ijg4MGY4YzVkZjY3NTI3ODE4NTZhMGQzNTYzN2FlNmQ5NWJmZGJkYzciLCJfc2Vzc2lvbl9leHBpcnkiOjAsIl9hdXRoX3VzZXJfaWQiOiIzIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-06 11:27:40.982459'),('cut3onqnj31gc3dxffwspzgt29vyr3ms','ZWI3ZDRhODJiNWFlZTFmMTFkNTQzZTM3M2FiMzQzYjhmNDlhMWM2ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-15 13:12:36.950973'),('cvtlnscd2jt0vdr8qnxiyltwg074s47z','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-03-03 12:09:08.623288'),('cwvius0kbqlpb15jmkypfbfiuu846r7y','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-03 11:40:32.583781'),('cwyno4poooshw99ohvwqmshp5bmcdro8','ZDVlMmQwY2JjMmI3MDVhNDg3NWRkM2UzNzEyNmQwYjBiYmUxODNmNjp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfc2Vzc2lvbl9leHBpcnkiOjAsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-04 12:06:39.242616'),('cylcu453d0uoy78meoa5e4mf3o5x8dsu','MDFkMGNmZTRiYTU2NGM2NmJlMWE5MTZhMGY5YjBmZGVkN2EzYjMwMDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-04-10 11:56:11.033697'),('cyt9z2kyl3dplwmnc14eggttv4p7hbmq','ZWI3ZDRhODJiNWFlZTFmMTFkNTQzZTM3M2FiMzQzYjhmNDlhMWM2ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-11 11:45:27.364312'),('d0grmcfne26rpyfjqkqghl9n0vgtovxm','NzE3NjViNDcyMWFhNGNiNzA0NjRjODU1ZmNhM2I1N2EzMDRjNTU2OTp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiNGIwMjY5YTAxMGUzOTI2YmY5MjUxZWJhOTkzYjIxNGVjOWQyZmRiMCIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-08 01:37:04.083267'),('d3mqzum2xo6s61ndhmidb1fyshcyp1l1','YjZjM2IyZjlkOTI4NTJlODZiMTRmNjljYzQ4YzYyMGEwYWVhOTg2Zjp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-02 07:09:50.527419'),('d8q80id0ceaqj3r9cs3uxdw47obp2b32','YWRkNWVlNGFlOWM5MWExZjg4MjdmY2M5ODI2MGYyZGY1NWJiMTUyODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-01-15 12:57:22.254600'),('d9fzu21hnrnfx0b4qhqj13uopbj6a9cz','NGE3MmNiMDYwODc2YzgxY2U2ZDVjYjQ3NDVjYTVhYmRiYjdiMGZlYzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-03-01 22:04:24.223001'),('daguxs0ocgntcetazcr992owkvz80mni','ZGIzYzAyMzJmOWM2MzY3NjU4NDdhMDc4M2E1NTM3NTk5ZTA4YWRiNzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-13 23:01:28.982908'),('dbnave63v5erm6wsz12clck6rzuo2irh','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-10 12:21:32.453337'),('debggplllbuns7yk9ke35xx98xsoqqlb','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-03-24 11:49:02.552457'),('dfi22dgv45yd4hcfhr1a7hvn766l8nhn','ZGIzYzAyMzJmOWM2MzY3NjU4NDdhMDc4M2E1NTM3NTk5ZTA4YWRiNzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-01 09:38:19.880020'),('diu3ohlfwxcu510fwm6qv75agbimfxdu','ODVjZDExOTVhYjRlNWQ1M2IwZTA0YjhiOTM4YmI3NDI0YjY1YzAwNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYifQ==','2018-05-19 09:26:25.153094'),('dkf5v5b8zrp06qpoeac3pbzctp1idqcn','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-05-23 12:39:24.462699'),('dpfee4x4x90n43kppyo2zqvh7bvr4ej6','ZGIzYzAyMzJmOWM2MzY3NjU4NDdhMDc4M2E1NTM3NTk5ZTA4YWRiNzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-14 12:09:58.009710'),('dr5loxtbk3prbb31g6x2polxmfxupvca','NmExYjFjYzU1NTBlMmVkMzhkZDdjYzFmODZjM2I1MjczMzgxMjllYzp7Il9hdXRoX3VzZXJfaWQiOiI1MiIsIl9hdXRoX3VzZXJfaGFzaCI6Ijc0YmIwY2U5NWU2ZTRhOGMxMTI4OWRlZmFjODJmYWNkOTRjM2EwZTYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9','2018-05-16 12:18:41.597907'),('e15j38brfrzcigtc5s7097b9lo0et8wu','YTg1OWE4ZDRiOGM4OTdlYjdhYzI4YTAxYmJkOTY5ZDU0NmZmNjhlNDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-03 11:02:13.332711'),('e1z764pvtig4x1zrqt7fr9adx8czj9a5','ZDcwODE3MzA5ZThlYWRhODk3MDFlZmVhMzQyNTg2ZDdlZGVmZjVmMTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-12-28 15:58:28.698963'),('e2044ph3dr1d0gaacujfclod9vdqcato','YTkwZDhiOWZiM2I0MmVkNjcxMGJlODdiY2QwZTAzZjcxMTE0YjA0MTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-01-06 15:07:55.003117'),('ed4q04hn7reu43xqc1tn66r0n57z8k6h','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-04 09:58:21.207893'),('ed6ltwbaqligusm5ko8j72t5txb9w9fa','ZGIzYzAyMzJmOWM2MzY3NjU4NDdhMDc4M2E1NTM3NTk5ZTA4YWRiNzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-01 14:26:47.633730'),('eeani1vei9q1mx660ga79grd4el20l34','YWRkNWVlNGFlOWM5MWExZjg4MjdmY2M5ODI2MGYyZGY1NWJiMTUyODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-01-15 12:41:47.147520'),('eg1yfgxsjq7ox09ljfthi35tjldrj0ck','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-04-02 12:02:32.853200'),('ehxuk2dfm82ll15k2t6xlo85kuotvz00','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-09 13:49:56.402246'),('emw58z9slxo5k3sc8bzlj9360jgx3b6f','YWRiYjdmMGQxYWRlN2JhMDNlNjU3OWQ2MjA0MTZmY2QzMmY1MWQwODp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-04-30 11:54:52.573615'),('eqigcuu18wlbfl50nyt5vhfcas0b362p','NDM2MTg1M2ZhYmRhMjY1ZDFmODM2ZTBkZWQwYjQ3MTE5ZjM1ZWE0Mzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZjk2OTE4MDkzZTYyYzFiYWVkYjYxYTExODgzY2U4NmU1OThkYWJjMCIsIl9hdXRoX3VzZXJfaWQiOiIyOCJ9','2018-05-03 20:12:54.348180'),('ew2z5yrvgdh9xgsmbx91o0apl732f9ww','NDQxYTczNTE0NDI4MmNmMWM2MmQ3MThjZTQzOTQ1ZmUzMGVlODU3Yzp7Il9hdXRoX3VzZXJfaWQiOiI0OCIsIl9hdXRoX3VzZXJfaGFzaCI6ImYwYzYyNWIwMWE4NWQ3N2I1OWQ1ODY2MTYzMTVhNTc3ZjliYjgwNDEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9','2018-05-23 20:39:57.907490'),('ezau4j0ltnm8futvee5zyqcqrt1cqcos','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-05-09 14:53:43.996169'),('f0qlfkh5rjgwqrnbhvtr1adz00jiuemp','OTgyYmY2YzlkY2ViY2NlMTY2YWM4OGYwODc4OWRiMzc1Y2E5NTUyYzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-12-31 04:41:25.157698'),('f3a78sdzcsjc19shc5tvww6kjwub4ccy','NTE0NGEyYTQwYjgxZGJlOTc5NjVjMjIyOGUyMGZiM2FiZDVmZjNlZjp7Il9hdXRoX3VzZXJfaWQiOiIyOCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZjk2OTE4MDkzZTYyYzFiYWVkYjYxYTExODgzY2U4NmU1OThkYWJjMCJ9','2018-05-03 19:00:38.709856'),('f5dnz59828h9s51zcwagw1cipahvn884','ZDIxODBhNzEzZWE3MzYyNDgyNGU1NzllMTI2ZWMyZGQzMGQ0NzA0Zjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYifQ==','2018-05-22 07:16:31.462423'),('f6jqxk9u8yw4fz50lymn8xd88p9aa9nh','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-03-17 09:07:16.005661'),('fb05vzeja08zcejsqc6dwbvx5dgv05vd','ZWI3ZDRhODJiNWFlZTFmMTFkNTQzZTM3M2FiMzQzYjhmNDlhMWM2ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-05-01 06:12:35.220407'),('fdeybneop8a882jelpdcf0rbzj4khitj','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-05-23 12:32:06.679743'),('fdi4zuoyre9nogmqlhh4fh87gu8hvypa','OTgyYmY2YzlkY2ViY2NlMTY2YWM4OGYwODc4OWRiMzc1Y2E5NTUyYzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-07 18:17:14.210565'),('fgb47beqeccc97mw7wbbssy4s6ghaeyi','ZDcwODE3MzA5ZThlYWRhODk3MDFlZmVhMzQyNTg2ZDdlZGVmZjVmMTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-12-29 08:18:07.230744'),('fik3hrpjqbvvemgdmg1j0er3f93ay9n8','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-05-01 14:42:37.381276'),('flt3ph9mdgwz9o0urc3w68xl9kzdskrc','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-21 15:24:56.135619'),('fm3og8pfmeitxo6lfbuerhqiijb6vgil','OWNhMTY2NmFhYmE4MjY1ZjdhZjEwOWM2YzRkZWVlZGNiNjk3OGIyODp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9pZCI6IjQ4IiwiX2F1dGhfdXNlcl9oYXNoIjoiYzFkYzBlZGY4Y2JjZDU4MDFiNjU1OTc3NWVkNmY4ZGRlM2Y5ZjQ1MyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-05-22 10:12:05.235383'),('fmlnofcgi89ll710r75v6hhj2l95pahm','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-10 13:02:13.187706'),('fo5aqt4h7t124cbjge1issvxotuvg4qx','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-22 07:33:31.784909'),('fpfan54dou07df8b57n5hmp6a3jr7uog','YTg1OWE4ZDRiOGM4OTdlYjdhYzI4YTAxYmJkOTY5ZDU0NmZmNjhlNDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-03 13:24:03.055596'),('fyd6s1n1ph7jezch0yitzcm8kkj9mvfr','YjkyNDIzNjIyN2MyMGRkNjhhYmZiNjU1YjQ1ZGE1Y2ZhMmZhMTZiYjp7Il9hdXRoX3VzZXJfaGFzaCI6ImY5NjkxODA5M2U2MmMxYmFlZGI2MWExMTg4M2NlODZlNTk4ZGFiYzAiLCJfYXV0aF91c2VyX2lkIjoiMjgiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9zZXNzaW9uX2V4cGlyeSI6MH0=','2018-05-21 11:14:33.767929'),('g04n02lvekwi1xbjgdipr7zhn9obhns9','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-03-03 14:07:13.282647'),('g0g3pfvg2e4e2gj3v6pru7mtgtaa5i0t','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-22 13:49:53.317011'),('g3sm0p5cjunlu9t003ysnpllxm4des3q','ZWI3ZDRhODJiNWFlZTFmMTFkNTQzZTM3M2FiMzQzYjhmNDlhMWM2ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-11 12:12:36.302585'),('g3t846vjguhud53453ah9x09wcniaqbf','ZDcwODE3MzA5ZThlYWRhODk3MDFlZmVhMzQyNTg2ZDdlZGVmZjVmMTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-12-30 03:42:40.351076'),('g7h5zt44ymts1qz4xd5id5xzpyog754c','YWRiYjdmMGQxYWRlN2JhMDNlNjU3OWQ2MjA0MTZmY2QzMmY1MWQwODp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-17 11:03:17.616238'),('gdld8jutz3xxxrwstavfz9xc1kfkp2tv','ZDcwODE3MzA5ZThlYWRhODk3MDFlZmVhMzQyNTg2ZDdlZGVmZjVmMTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-12-28 15:59:06.940734'),('gfm68qrqpgkmjoh4ra2m2x4yfnme7xhv','ZjYzZWQ1NzMzMWZlNTNlOGM1YWRiNTAzYmEzYzlmYTRhY2UxMWNhZTp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-10 20:54:01.408215'),('ggwulyx876qx42366hrg4nx0vknzrkrv','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-10 09:54:08.534084'),('gi56lhre5ct79fn8c32nx04p0yrnz1k4','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-22 06:13:51.073401'),('gixpggh5txz039wkikqkd25ddl4haj7o','M2NlOTc5ODI1YTM0NmVmMzY5MTFjZWIyOWE2M2Q3NTI4ZjMwZmVlMzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQ4IiwiX2F1dGhfdXNlcl9oYXNoIjoiNWRkMWJlZmQxMzcyYzEzODkzMGQ3YzA3ZTZmNzU1NjNmOGE5ODIwNCIsIl9zZXNzaW9uX2V4cGlyeSI6MH0=','2018-05-21 12:34:07.857837'),('gnw3cxmu30ub7d3wuugpuo7x4g5ef7sl','OTgyYmY2YzlkY2ViY2NlMTY2YWM4OGYwODc4OWRiMzc1Y2E5NTUyYzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-12-31 01:44:49.224919'),('gp2yb339d845qvcpt7pppu8u918e0rf7','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-03-26 06:09:24.731879'),('grx8qk20xb9ti7vjunbv1kg5fzv7yaqm','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-05 12:47:33.175262'),('gtdma7etw8vy8cnaj38n13xx3v40lm02','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-05-16 07:29:17.792871'),('gzfjh9hbhheyq5iji1p9ihemu9n29itu','NjE4NjNiNzNkNWFiY2NhYmU1NDVkM2NiZDNmYzliYjNlY2ViMzM5Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-01-02 14:39:21.784693'),('h278b8g8z1dayn609sjxxk7uc8cos2rh','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-04-30 06:00:10.665380'),('h2zcnzfb8jz8m8uw8sij3t0wrum5q5y1','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-04-07 13:08:31.802231'),('h4k9fxuhbh5a9rxtxl9cas323xsthcp8','OTgyYmY2YzlkY2ViY2NlMTY2YWM4OGYwODc4OWRiMzc1Y2E5NTUyYzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-07 16:54:47.109970'),('hgkqswz9megbtz6e7k1uvq305ggczycs','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-05-23 17:16:16.998489'),('hlfb0i7avy0ltkyykj4amv7szw9oazl4','YzU2OGEwNzRkMDNmMjI4MDA0ODc4MDVhZGVlMTVhOTk1MTIwMGFjOTp7IndpemFyZF9jb250YWN0X3dpemFyZCI6eyJzdGVwX2RhdGEiOnt9LCJzdGVwX2ZpbGVzIjp7fSwic3RlcCI6IjAiLCJleHRyYV9kYXRhIjp7fX0sIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-15 10:13:58.185043'),('hly672uwqc897iskoi284ue03mjakr0d','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-04-25 15:00:25.539875'),('hq5p8mska9mcad20h3nqgr0s8k1fkh73','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-06 06:21:28.496691'),('hrmp0wyaf3crh1xqtpla02x34x3dqw6f','NTE3MTZkN2RmMmM1MWNmYmMwZDAzODBmNTMwNjcxNDE0OGJhYTAyNDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfc2Vzc2lvbl9leHBpcnkiOjAsIl9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYifQ==','2018-05-24 06:54:20.536530'),('hry0ymqw75x07opnwyu4hm5n6fuhpopr','YmE4NGZlNTI1OTk5NDBmYzE3YzIzZjBlZDhiM2JjZDgyNzk5NzQxNTp7IndpemFyZF9jb250YWN0X3dpemFyZCI6eyJzdGVwX2RhdGEiOnt9LCJzdGVwX2ZpbGVzIjp7fSwic3RlcCI6IjAiLCJleHRyYV9kYXRhIjp7fX19','2018-05-13 21:36:44.420007'),('hsmbgz51ve2ld96ty914pplati37aeky','YTkwZDhiOWZiM2I0MmVkNjcxMGJlODdiY2QwZTAzZjcxMTE0YjA0MTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2017-12-30 08:02:29.921239'),('hufsqzoivjfiub2etzijf9i6q3sdypse','ZDcwODE3MzA5ZThlYWRhODk3MDFlZmVhMzQyNTg2ZDdlZGVmZjVmMTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-12-29 06:32:40.228113'),('hv58zppnt3xoi906za6w79i5hw0u1muj','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-25 13:17:55.388669'),('i76qdfg14kx4y8ti0z8qiu9x4bozt4uq','ZGIzYzAyMzJmOWM2MzY3NjU4NDdhMDc4M2E1NTM3NTk5ZTA4YWRiNzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2017-12-31 11:18:59.036757'),('i8g1ywmr0zvo94rm5ndvl4cto17jfobs','NjMwNGE4NjE1ZDUxOTZiNGY4YjVhY2MxMWJjMjc5YTM2ZDM3MGEyODp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQzIiwiX2F1dGhfdXNlcl9oYXNoIjoiZjExNTE0NWJmOThjY2I2ZjEwNDFjOTAzOWNkNmY1NDYwMGJiZWZlYSIsIl9zZXNzaW9uX2V4cGlyeSI6MH0=','2018-05-21 11:40:17.128663'),('i8r0vcw4ptr0q4d3meud1n8n4ddx018p','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-04-15 14:28:06.969347'),('ifxc7k8jgylnswnb94m3f63w4x6pmj71','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-21 10:53:54.047744'),('iky387z3yljrzz49publzbx9yls9txd3','ZDcwODE3MzA5ZThlYWRhODk3MDFlZmVhMzQyNTg2ZDdlZGVmZjVmMTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-12-28 21:41:22.577535'),('impz0w1badvp8tt0hpdfhh0wvenmrhf9','ODVjZDExOTVhYjRlNWQ1M2IwZTA0YjhiOTM4YmI3NDI0YjY1YzAwNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYifQ==','2018-04-07 13:15:26.699525'),('intpz8x87l68aocxfecpo7b0mot04t2n','ZGY1YmU5MTQ1ZGFhMjQ5ZjNiYjk5ZjI4NzFjYzE4MjMwYTM4ZjRlZjp7Il9hdXRoX3VzZXJfaGFzaCI6ImYxMTUxNDViZjk4Y2NiNmYxMDQxYzkwMzljZDZmNTQ2MDBiYmVmZWEiLCJfYXV0aF91c2VyX2lkIjoiNDMiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9','2018-05-07 10:10:10.959607'),('isv3qdbd6s4f6o4vgl8tdosyj3mmj18s','YWRkNWVlNGFlOWM5MWExZjg4MjdmY2M5ODI2MGYyZGY1NWJiMTUyODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-01-15 16:15:22.922458'),('j07r3wokmqywm7d23luihmjsmn28a0rt','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-05-03 19:31:28.384446'),('j5m77q9x9hd92p42efke870fcczrcrvd','ZGIzYzAyMzJmOWM2MzY3NjU4NDdhMDc4M2E1NTM3NTk5ZTA4YWRiNzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2017-12-31 10:48:28.632874'),('jawm63rkqovguszpt2y0fqwxvsbd3hrp','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-03 06:06:20.670424'),('jcgs6ge3y7rtgdcem21a9igwgtjpzwfn','NDM2MTg1M2ZhYmRhMjY1ZDFmODM2ZTBkZWQwYjQ3MTE5ZjM1ZWE0Mzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZjk2OTE4MDkzZTYyYzFiYWVkYjYxYTExODgzY2U4NmU1OThkYWJjMCIsIl9hdXRoX3VzZXJfaWQiOiIyOCJ9','2018-05-03 16:15:29.792245'),('jhd6ndthftbxdnsxpehou0oymjvrmm8x','YjM4NmZjMWE3YTVlZTYxM2U0MDUxZTg1ZjllOWE1M2NiY2E2N2RhMTp7Il9hdXRoX3VzZXJfaGFzaCI6ImZlYmVjOTM2N2QzYmNiMzEyYjgyZTllYmFkMzZlN2RjNjA2YmM0ZDUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI0MiJ9','2018-05-22 10:57:35.305883'),('jo48foph0ekx9pz2de2dnh2yj0r756pi','YjZjM2IyZjlkOTI4NTJlODZiMTRmNjljYzQ4YzYyMGEwYWVhOTg2Zjp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-02 06:38:29.895190'),('joq7tsmgezcvnvwmat81yg12lkk4pvkg','NTE3MTZkN2RmMmM1MWNmYmMwZDAzODBmNTMwNjcxNDE0OGJhYTAyNDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfc2Vzc2lvbl9leHBpcnkiOjAsIl9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYifQ==','2018-05-22 07:06:05.368384'),('jum0iklr7nh9aaksdjw43ascau7o1jmy','YWRiYjdmMGQxYWRlN2JhMDNlNjU3OWQ2MjA0MTZmY2QzMmY1MWQwODp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-05-01 08:50:36.610954'),('jwnx48b6uharwjxb1hzlth21le6btm2p','ZDMyMmJlOWIxYzE2Y2JmOTBhYjJkMWY1NTI0NTM5ZmY0YmU3MjExZjp7Il9hdXRoX3VzZXJfaWQiOiI1MCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2hhc2giOiI0NTkwZTI0MzliYWQ1NDcwMDg0YmU1Y2YxOWZhYTc0OGVmZWJhZDQ5In0=','2018-05-22 06:55:00.522401'),('jyb4nt103mxcka57bi1022tps57u2u5g','ZWI3ZDRhODJiNWFlZTFmMTFkNTQzZTM3M2FiMzQzYjhmNDlhMWM2ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-08 11:16:20.199571'),('jymvcnrfwue9zkfns2101s2d7wzawb7p','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-10 16:50:42.094577'),('k0r6brgrz65sc23axko2qtlqy6wa1kow','ZWI3ZDRhODJiNWFlZTFmMTFkNTQzZTM3M2FiMzQzYjhmNDlhMWM2ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-15 13:26:56.137108'),('k1p5e83s2urejl8a54jcjg6zdo9ueapr','NzE3NjViNDcyMWFhNGNiNzA0NjRjODU1ZmNhM2I1N2EzMDRjNTU2OTp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiNGIwMjY5YTAxMGUzOTI2YmY5MjUxZWJhOTkzYjIxNGVjOWQyZmRiMCIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-08 00:58:36.886565'),('kcoaaqyvjj7o16izk2hx7xnkp6m2l2ft','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-17 12:04:57.463235'),('kdn0v2wnue36n6616k8uk42u812t2d43','ZGIzYzAyMzJmOWM2MzY3NjU4NDdhMDc4M2E1NTM3NTk5ZTA4YWRiNzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-01 06:03:27.917056'),('kgu0bvmej954fhrekjxqk5azn8vi0548','ZWI3ZDRhODJiNWFlZTFmMTFkNTQzZTM3M2FiMzQzYjhmNDlhMWM2ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-08 06:00:33.476213'),('kpp6sapbftsftfltjx74kqo1woyf6wxe','OWVmYTM4NGYzYzdkYzczZTcxMmU4MzE4NmZkM2JjZjY0NTUzZmViODp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMzc3YTVjMmJiMDljYjA2MGU4ODQ2NTAwZWYwZjdmYzQzZjBiOWUyMCIsIl9hdXRoX3VzZXJfaWQiOiIyNCJ9','2018-04-30 11:33:53.660573'),('kuc1v6n0pt1k1rubrgfz4c0p02ntv0lq','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-20 11:38:27.137830'),('kuuwgdftvkizjy023m98tx2qlk12lsqa','ZDcwODE3MzA5ZThlYWRhODk3MDFlZmVhMzQyNTg2ZDdlZGVmZjVmMTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-12-30 05:22:10.427243'),('l0enlcjfg2ao5viyzp82ka63rlcjyx8t','ZjAwODk3OWExYWQ0OTUyYTdiYTQ4N2Q5ZTgwNjYzYzVjOTExOGI3Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-12-31 13:02:07.766718'),('l43ebnnwko0i7cjom1qc34qptsida7ar','MTc2ZGE4NjNkM2E0N2FiNDMxYTVmYTJjZjFkYjRmNWFhM2NlMTZjNTp7Il9hdXRoX3VzZXJfaGFzaCI6ImZlYmVjOTM2N2QzYmNiMzEyYjgyZTllYmFkMzZlN2RjNjA2YmM0ZDUiLCJfYXV0aF91c2VyX2lkIjoiNDIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9','2018-05-24 10:12:51.352998'),('l49mhbn8qm00gqq90bh5jljk8q2fd86k','ZmQzZDc0MDI2ZDIyMWQwNDBkMmFkNjJkZjZjZDg2NjExYjhiODBmMzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQzIiwiX2F1dGhfdXNlcl9oYXNoIjoiZjExNTE0NWJmOThjY2I2ZjEwNDFjOTAzOWNkNmY1NDYwMGJiZWZlYSJ9','2018-05-08 08:25:23.370906'),('l59ip6eugre7n4ov88ebz3e7fdyrxr85','YWRkNWVlNGFlOWM5MWExZjg4MjdmY2M5ODI2MGYyZGY1NWJiMTUyODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-01-16 19:17:33.420482'),('l5gtycmmhir6hhcp9klkturx14y523ss','ZjlmYWNiZDk1Yzc0ZWUyYTU4NjM5NWE1MmVlY2Y2YTYwYjFmNGVhZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjUwIiwiX2F1dGhfdXNlcl9oYXNoIjoiNDU5MGUyNDM5YmFkNTQ3MDA4NGJlNWNmMTlmYWE3NDhlZmViYWQ0OSIsIl9zZXNzaW9uX2V4cGlyeSI6MH0=','2018-05-21 12:35:54.940923'),('l67y5sjg8qajijtgdot4hj8g8j91asgl','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-15 14:37:03.730268'),('l7xuznbgz8bzfvjl5k58qxdgd8jcpajy','NjE4NjNiNzNkNWFiY2NhYmU1NDVkM2NiZDNmYzliYjNlY2ViMzM5Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-04-15 14:11:36.382330'),('lda7m3cedognpcb97u84y3i7z5kjwzwq','ZjhmMzRjMWMyYmQ0OTc2OTU4ZjQ3OTZkYjZlNDNmNzQzZWQ4N2RhNTp7IndpemFyZF9sb2dpbl93aXphcmQiOnsic3RlcF9kYXRhIjp7fSwic3RlcCI6IjAiLCJleHRyYV9kYXRhIjp7fSwic3RlcF9maWxlcyI6e319fQ==','2018-05-14 14:11:48.190487'),('leh38dcrf02abmdcx11rdeqyacp09xg1','NzE3NjViNDcyMWFhNGNiNzA0NjRjODU1ZmNhM2I1N2EzMDRjNTU2OTp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiNGIwMjY5YTAxMGUzOTI2YmY5MjUxZWJhOTkzYjIxNGVjOWQyZmRiMCIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-08 01:28:25.353380'),('lep3ozjwtx4pgb97wqewys1lcwaghjze','YTJmNGFlOGI2NjExYWZjMDZmZWE3MmRjZjQyNzg4OTVmYTJjYTFhMzp7IndpemFyZF9jb250YWN0X3dpemFyZCI6eyJzdGVwX2RhdGEiOnt9LCJzdGVwIjoiMCIsInN0ZXBfZmlsZXMiOnt9LCJleHRyYV9kYXRhIjp7fX19','2018-05-15 12:13:38.639546'),('lf380et76tay9dn27rjnpbfnj8pwxcfd','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-02 09:36:16.781723'),('llruyahd6lytcpqgaejph5ovw922al7o','ZGIzYzAyMzJmOWM2MzY3NjU4NDdhMDc4M2E1NTM3NTk5ZTA4YWRiNzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-14 12:11:37.693500'),('lluyqezz6juz5kfzbsry41fn9c8txlpw','MmQzYTExNmM4OTgyMDhhMjBmZmMwMDc5NzkyMWMyNzE0ZTYwZjljODp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYifQ==','2018-03-22 13:52:51.883607'),('lpr69uvtnzun3c04if5hqi8xfabkrwsr','ZGIzYzAyMzJmOWM2MzY3NjU4NDdhMDc4M2E1NTM3NTk5ZTA4YWRiNzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2017-12-31 14:47:20.694139'),('lsb4h4kgfn2qw15cy50syhhv380ynkhh','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-21 08:11:33.252566'),('ltsvyesi2dn76xj18cem0159q9ghyn01','YjZjM2IyZjlkOTI4NTJlODZiMTRmNjljYzQ4YzYyMGEwYWVhOTg2Zjp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-03 10:04:41.760321'),('m0bl523z5rillqbhqinrzopeiknsbodl','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-08 08:02:11.280069'),('m0ihu7bhsayjwyqsdrgei87urx9v2hrp','NjE4NjNiNzNkNWFiY2NhYmU1NDVkM2NiZDNmYzliYjNlY2ViMzM5Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-01-14 11:38:40.040605'),('m4rjzhta7bgtt9dzppbpu01bl3ex5vj6','M2ZhYjA2ZDRiMWMyMWNjZGFmMWQ3OTM5NGE1NDBlMjU3MTIzNmI5YTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwid2l6YXJkX2xvZ2luX3dpemFyZCI6eyJzdGVwX2RhdGEiOnt9LCJleHRyYV9kYXRhIjp7fSwic3RlcF9maWxlcyI6e30sInN0ZXAiOiIwIn0sIl9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJ3aXphcmRfY29udGFjdF93aXphcmQiOnsic3RlcF9kYXRhIjp7fSwiZXh0cmFfZGF0YSI6e30sInN0ZXBfZmlsZXMiOnt9LCJzdGVwIjoiMCJ9LCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9','2018-05-15 08:19:10.246492'),('m9iwdvrcyp0gr7om7h1q30kjbqxxw4i6','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-03-25 08:41:39.110802'),('ma5rhijqajc6hbzafkcli22b105pdz42','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-07 14:08:23.884571'),('mg07vf4dgrclbk8e6p6n58h2skvl9v07','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-26 08:30:44.290588'),('mhykhpgecq32dfszq0861nl9ouurfxer','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-03-03 15:02:25.801728'),('mjxtsxqujmuy2dsp1ln6k2n09o8mbci4','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-04-02 06:45:01.326152'),('mm7abgnqlii66hfz5uim0gnwtq7iirzg','ZGIzYzAyMzJmOWM2MzY3NjU4NDdhMDc4M2E1NTM3NTk5ZTA4YWRiNzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-14 12:08:51.380612'),('mtx3l0gzvuthoru9eyf407f9vsj22u70','ZjYzZWQ1NzMzMWZlNTNlOGM1YWRiNTAzYmEzYzlmYTRhY2UxMWNhZTp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-09 03:39:11.420336'),('mveapufxr9t0fgfp16j77ib2hsqjzmus','ZWI3ZDRhODJiNWFlZTFmMTFkNTQzZTM3M2FiMzQzYjhmNDlhMWM2ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-03-29 13:20:21.705719'),('n2btnj8dh6pzitvzj168gj859judklh3','Njc0MzQyOTU0NjJiNjcxYzFiOWFhMjlkMjVlZTBiZmYwOGJhMTg1MDp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2017-12-31 06:35:22.808854'),('n2bxrg3ukylp95sdw9g18aodlu264x2u','ZGIzYzAyMzJmOWM2MzY3NjU4NDdhMDc4M2E1NTM3NTk5ZTA4YWRiNzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2017-12-31 07:25:24.832810'),('n2ckc89btgdqxz4cttfglt7earw13ecj','YjZjM2IyZjlkOTI4NTJlODZiMTRmNjljYzQ4YzYyMGEwYWVhOTg2Zjp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-01 17:45:47.581556'),('naao0oe0l22u7roo7nw44conx9gnlrsi','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-05-02 09:28:41.663086'),('nccp70j8hf86y5dq7aj1m9pfm1p444mj','YWRiYjdmMGQxYWRlN2JhMDNlNjU3OWQ2MjA0MTZmY2QzMmY1MWQwODp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-24 11:28:31.015479'),('nkpqe7765kgijrvjlwmahhtyc8t6t7pb','ZjAwODk3OWExYWQ0OTUyYTdiYTQ4N2Q5ZTgwNjYzYzVjOTExOGI3Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-01 12:49:20.412942'),('nl85hrni75uv8z1fwui6vizw2lkbokx0','ZWI3ZDRhODJiNWFlZTFmMTFkNTQzZTM3M2FiMzQzYjhmNDlhMWM2ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-10 15:15:10.596171'),('nm9mhistw1otepb4fdnnp0j8puvm8yuu','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-03 22:27:43.225726'),('nmd9ooslg87t1d9to41oro5jqhn1i64u','YTkwZDhiOWZiM2I0MmVkNjcxMGJlODdiY2QwZTAzZjcxMTE0YjA0MTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-01-06 15:02:12.518212'),('nn35ulys8hzegvsgkedtvc8es6gn225k','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-24 12:38:13.634919'),('ns9jusej8pou0x9hfo9d7bugi72m1lji','ZDVlMmQwY2JjMmI3MDVhNDg3NWRkM2UzNzEyNmQwYjBiYmUxODNmNjp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfc2Vzc2lvbl9leHBpcnkiOjAsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-05-17 13:52:29.393166'),('ntop88yuktm4zvuln28f4tgqcbkp4896','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-22 07:52:30.001953'),('nuwm9mcx5ljl6sh5ascfxgammcmciw9o','ZWI3ZDRhODJiNWFlZTFmMTFkNTQzZTM3M2FiMzQzYjhmNDlhMWM2ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-05-22 13:39:09.200004'),('nxilrji3ghn8t2bqq2s0cjmnkrusjp4u','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-05-23 17:19:36.480152'),('nyk7hshnvidbx9wr4ygujfmyyyk18so3','ZWI3ZDRhODJiNWFlZTFmMTFkNTQzZTM3M2FiMzQzYjhmNDlhMWM2ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-05-23 20:16:13.319162'),('nznyifhuy67uvt2qsdmmd5q07vqfqmf4','YWRiYjdmMGQxYWRlN2JhMDNlNjU3OWQ2MjA0MTZmY2QzMmY1MWQwODp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-04-29 05:51:23.551491'),('o38ef1jfwp3s5cb3j2nh3pyfwk9dul2i','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-02-28 13:32:55.218034'),('o4st66i6qdg202e69dtbe03yn79to4o8','YWRiYjdmMGQxYWRlN2JhMDNlNjU3OWQ2MjA0MTZmY2QzMmY1MWQwODp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-27 05:30:57.834598'),('o7vsiu2m1k3thx38aysoy4euz8ye282c','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-27 12:24:43.272134'),('odsmipzaat5t4d859kb9voxeew5ayp94','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-03 15:36:18.011934'),('oequ5pi2is6246bpgkyuzat3mpw62xgu','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-04-30 13:14:42.907126'),('of0sphdc2u6s42sc9ldq8nvc4ugt1wfy','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-05-08 19:35:41.362630'),('ogw1jp0svthox2pzt9ya61znlh31ris2','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-04-16 12:09:18.696958'),('orbwicbjoag8tdobxw5pqbet51v02clg','ZGIzYzAyMzJmOWM2MzY3NjU4NDdhMDc4M2E1NTM3NTk5ZTA4YWRiNzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-05-17 13:50:24.700775'),('ornigng7ib7rjk0inbdqgfvwpg6xv5ov','OTM0Zjc4Yjc0ZmIwMGMyODc5NTcyY2E5NjI1ZTBlNzg2ODMyYzlmYTp7Il9hdXRoX3VzZXJfaGFzaCI6Ijk3YjdkMDQ0YTc3YTZjNGMzMGJiNjQwOTlhZmNkMmU1OTdlYzQyN2YiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIzMCJ9','2018-05-06 20:03:07.276644'),('owv2s858cxba6xook9xrbgz1gb05myij','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-04 00:47:58.351116'),('p2nfijydz6r567191k2argofrj0entjc','ZDcwODE3MzA5ZThlYWRhODk3MDFlZmVhMzQyNTg2ZDdlZGVmZjVmMTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-12-28 16:00:28.157285'),('p4u7t088n476it5ut09e6fzl7hz87do5','ZWI3ZDRhODJiNWFlZTFmMTFkNTQzZTM3M2FiMzQzYjhmNDlhMWM2ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-10 14:48:20.030739'),('p4wpk8fwnk0qy22a57pf0omg6whx51sg','ZDVlMmQwY2JjMmI3MDVhNDg3NWRkM2UzNzEyNmQwYjBiYmUxODNmNjp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfc2Vzc2lvbl9leHBpcnkiOjAsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-04 06:07:50.132763'),('pbwexj681zn4wdq4vjagi2muq4j1zcd2','ZDcwODE3MzA5ZThlYWRhODk3MDFlZmVhMzQyNTg2ZDdlZGVmZjVmMTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-12-29 08:16:20.893099'),('pfct0p1dcl0poyz03rnjy54d17yqg029','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-04-12 11:59:40.768740'),('pfz6etepbaqr4omhzf8b7o23xbngpxbm','YWRkNWVlNGFlOWM5MWExZjg4MjdmY2M5ODI2MGYyZGY1NWJiMTUyODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-01-17 16:10:23.539216'),('ppnyajwnoo6yyxf1z8tbi3vrhxl8uuxc','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-09 13:32:56.900281'),('pr2pxoa4l38ttkkw5a2p82xfm8kpkzu5','ZWI3ZDRhODJiNWFlZTFmMTFkNTQzZTM3M2FiMzQzYjhmNDlhMWM2ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-05-23 20:14:24.679165'),('ps9bilgjbbm2b7o9su7gbie0ylw02atw','YWRiYjdmMGQxYWRlN2JhMDNlNjU3OWQ2MjA0MTZmY2QzMmY1MWQwODp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-24 11:02:09.465420'),('q07x1vfrdu506s0iag030exvb70cncsd','YWRiYjdmMGQxYWRlN2JhMDNlNjU3OWQ2MjA0MTZmY2QzMmY1MWQwODp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-17 10:39:17.220271'),('q1pet7a0plbgjf7ujcr9awkx0fofi83x','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-04 06:17:16.939698'),('q39gs8v0sf46p3hj3p623fp3ectpbs3z','NzI0YjllMDkzNmM5NjBjNGQ3OTM1NjQ2NTY0MTYzZWRlNmM1YTY2YTp7Il9hdXRoX3VzZXJfaGFzaCI6ImY5NjkxODA5M2U2MmMxYmFlZGI2MWExMTg4M2NlODZlNTk4ZGFiYzAiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIyOCJ9','2018-05-07 08:18:48.094232'),('q67bv2qr5uktmk99ytqkxoj9tkt2dlrr','MDNhNGIyYWJjNzgzYjZlZDA0MGJlMGRkNGZhOTBlODI2YzgwNWUwZTp7Il9hdXRoX3VzZXJfaWQiOiI0OCIsIl9hdXRoX3VzZXJfaGFzaCI6IjhiMWJjNGJjYjUzN2NkMWU3OWQ2N2Y4MTk5NzQ1Y2RhOTQwMTAzZGEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9zZXNzaW9uX2V4cGlyeSI6MH0=','2018-05-16 07:58:19.611146'),('q6sf5iqxnbhoutr70drw5mycdncgru6r','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-23 21:24:02.045628'),('qbxgb30f0p2ypwqs2uxvhxdwfbav9ylj','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-15 07:06:27.409555'),('qc0l8ulxq1bphf9kf2fbmtxvdsy7a3z2','MmQzYTExNmM4OTgyMDhhMjBmZmMwMDc5NzkyMWMyNzE0ZTYwZjljODp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYifQ==','2018-05-22 07:41:10.416295'),('qiszd9blwvp1xg4qz21syjyf3ntwvim3','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-05-25 04:40:01.431163'),('qlrkqsutenzvqwuhbb05l2sq6q0j11cj','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-12 14:48:08.846411'),('qq668p02jpkxhphenlv0tz1nm939nfrz','NzE3NjViNDcyMWFhNGNiNzA0NjRjODU1ZmNhM2I1N2EzMDRjNTU2OTp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiNGIwMjY5YTAxMGUzOTI2YmY5MjUxZWJhOTkzYjIxNGVjOWQyZmRiMCIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-08 00:54:24.250299'),('qvxi4686er2xwoy4ldiws5xwn08nkvcp','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-21 08:01:22.928422'),('r0hglrrdfcs3ovi34p4ux2yz7ckz6q8h','NTE3MTZkN2RmMmM1MWNmYmMwZDAzODBmNTMwNjcxNDE0OGJhYTAyNDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfc2Vzc2lvbl9leHBpcnkiOjAsIl9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYifQ==','2018-05-02 11:04:21.517340'),('r7fywluhkx0oant3b6p2qc5vsueda6fp','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-15 13:02:13.276297'),('r7tpqrxvgh3hhysxth1bza0089bsrc9g','Y2FkZDlkZDIwZGM5YzlhNzZlMTIxMDE2NmZmNjMxYTg5ZThiMmQ0ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjI4IiwiX2F1dGhfdXNlcl9oYXNoIjoiZjk2OTE4MDkzZTYyYzFiYWVkYjYxYTExODgzY2U4NmU1OThkYWJjMCJ9','2018-05-05 19:35:31.173705'),('r7wlf13cx1kft0bz02e3c6j4dx9teigu','OTgyYmY2YzlkY2ViY2NlMTY2YWM4OGYwODc4OWRiMzc1Y2E5NTUyYzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-12-31 06:49:51.562671'),('r96z9g552gh5h7mzfxdbb9yymi20hg3y','ZGIzYzAyMzJmOWM2MzY3NjU4NDdhMDc4M2E1NTM3NTk5ZTA4YWRiNzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2017-12-31 12:19:26.372605'),('rar4rikzoqj2lx14t2u3z8c2e6x8solv','YWU0MWJiOTM0NTZjMzY2MDJlYjE4ODM2YjVlNzg5MjI3MjNhMTQ2Yzp7Il9hdXRoX3VzZXJfaWQiOiI0OCIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZjBjNjI1YjAxYTg1ZDc3YjU5ZDU4NjYxNjMxNWE1NzdmOWJiODA0MSJ9','2018-05-23 06:13:20.032542'),('rc41dmc3shvsfxsnnip0hkexaldmgeom','ZWI3ZDRhODJiNWFlZTFmMTFkNTQzZTM3M2FiMzQzYjhmNDlhMWM2ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-08 09:36:06.984068'),('rcahb6vndiyjytgd953325si296lb975','ZGIzYzAyMzJmOWM2MzY3NjU4NDdhMDc4M2E1NTM3NTk5ZTA4YWRiNzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2017-12-31 10:51:45.286541'),('rje56ihpuajkc31kxktmm208ryu5iqb6','ZGIzYzAyMzJmOWM2MzY3NjU4NDdhMDc4M2E1NTM3NTk5ZTA4YWRiNzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2017-12-31 13:52:55.207957'),('rng0ruyxgfb6t4cems2azg162e51abp9','ODZkYTk2MDA0NmQ2OTJlZGM5MTcwNWYzNDIyMjZlZjYyNDA1OGUyNjp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfc2Vzc2lvbl9leHBpcnkiOjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-05-16 09:01:33.701707'),('rqtiy6kjd8tjef40awp2p27qep5kfrrs','YWRiYjdmMGQxYWRlN2JhMDNlNjU3OWQ2MjA0MTZmY2QzMmY1MWQwODp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-17 10:41:09.501139'),('rzl07fhrp2uc6hi429gt2703q60oo760','MmRmZWJhOTYxOTk5ODc4YzhkNzRmZTJlNTA0NDJmZjBhMGIxNjZlNTp7Il9hdXRoX3VzZXJfaGFzaCI6IjQzNTRjODhiZWU5N2UxODYyOWY2OWVhMTU4YTQ1MWY1N2JkOWJiNGUiLCJfYXV0aF91c2VyX2lkIjoiMjgiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9','2018-05-23 06:43:57.064247'),('s1iv5mksprkhrcosp5efe29602dtu4lf','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-23 15:50:16.662504'),('sk7g662kn047b9py8iqmsq3lnh7j02ht','YWRiYjdmMGQxYWRlN2JhMDNlNjU3OWQ2MjA0MTZmY2QzMmY1MWQwODp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-20 08:23:03.160680'),('sk9ioy6sjtgo023nmhxcsywxxuenr5zp','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-21 12:41:16.342021'),('skhe0orllgszu7rjr48np88vmyzwutmk','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-08 11:40:39.106709'),('sma8884bg7gg05ubkx7yp9rsg8qexbai','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-04-02 13:06:48.910497'),('snnyyvktc5j85cwjizd9lyxh7xe25hji','ZDVlMmQwY2JjMmI3MDVhNDg3NWRkM2UzNzEyNmQwYjBiYmUxODNmNjp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfc2Vzc2lvbl9leHBpcnkiOjAsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-04 11:43:45.480388'),('so8k7nis4bpvmt6nyvbjcwgyv898nkaq','NDMzMWY4YmQ0OGQwYTAwMTM2OTgzZTAzNmU1OTI4ZmIzZDFhY2I2Mjp7Il9hdXRoX3VzZXJfaWQiOiIyOCIsIl9hdXRoX3VzZXJfaGFzaCI6ImY5NjkxODA5M2U2MmMxYmFlZGI2MWExMTg4M2NlODZlNTk4ZGFiYzAiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9','2018-04-30 20:00:04.348312'),('sqj8bqoxuqd1kwaq68be6xuba5hos7wj','ZDVlMmQwY2JjMmI3MDVhNDg3NWRkM2UzNzEyNmQwYjBiYmUxODNmNjp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfc2Vzc2lvbl9leHBpcnkiOjAsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-04 17:29:19.238791'),('sto3802vx69kaop9lvd7k9ul7vqstqo7','MmQzYTExNmM4OTgyMDhhMjBmZmMwMDc5NzkyMWMyNzE0ZTYwZjljODp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYifQ==','2017-12-30 06:16:48.740997'),('szl316euve84bwq0s57f72vc8jsqs5uh','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-03-10 11:53:51.036833'),('t25p02f02deigbmuni0ly0qlpiu923q8','ZDVlMmQwY2JjMmI3MDVhNDg3NWRkM2UzNzEyNmQwYjBiYmUxODNmNjp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfc2Vzc2lvbl9leHBpcnkiOjAsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-04 14:02:12.128698'),('t4mv6zkdqzfqqww3ugki1mmm1gz4lchr','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-04-16 13:22:22.341386'),('t5k7ftcr0f7cyc82uku3qv986c7auylp','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-05-22 11:01:47.283504'),('t6i855f5nuyyuvesvz2w78ue6y22yoq7','YjkyM2RkMDY1YTNmNjUxNmFjZWVmODMyY2YyZWU1ODRjYzYzZjhmOTp7Il9hdXRoX3VzZXJfaWQiOiI0MyIsIl9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJmMTE1MTQ1YmY5OGNjYjZmMTA0MWM5MDM5Y2Q2ZjU0NjAwYmJlZmVhIn0=','2018-05-22 09:27:43.115882'),('t7d64tp2mi5y32ltvtooeuvsbhm3g1im','MGViZGIyNTdhM2EwODVmOTljZmFjOTBlY2E2M2U4YmY1ZDEyMmFmNjp7fQ==','2018-04-23 08:51:23.577673'),('te70b03e27kpzh2106kgnsd5lk9ivu2n','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-23 20:37:52.233359'),('tgrtnbxsh2scnspd568oyactm919mxlh','ZWI3ZDRhODJiNWFlZTFmMTFkNTQzZTM3M2FiMzQzYjhmNDlhMWM2ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-11 13:39:21.981982'),('tgzmg9fl8p0uo8255kc6x4a9ozlukb00','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-07 17:10:56.994693'),('tjr4qgordu8unvuf7iug7hbu3m37bi3j','ZDcwODE3MzA5ZThlYWRhODk3MDFlZmVhMzQyNTg2ZDdlZGVmZjVmMTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-05-22 09:53:11.195657'),('tpawkrkzo1b5q7de7uuqvavhg0ricsvu','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-18 14:07:55.498495'),('trxdo3ta99qr5awoda2q2fw89neokfm2','YWRkNWVlNGFlOWM5MWExZjg4MjdmY2M5ODI2MGYyZGY1NWJiMTUyODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-01-21 12:52:07.613395'),('tsbcov4xw5t8pwu3vq86arwo2bcvvbrd','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-26 13:24:09.684390'),('tsrrdhja309gdmrg011a9m448dcuf9tv','NjE4NjNiNzNkNWFiY2NhYmU1NDVkM2NiZDNmYzliYjNlY2ViMzM5Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-01-17 07:18:14.502823'),('tuwoqcbaztcj5wqfohxmjrawsprclmqw','ZDVlMmQwY2JjMmI3MDVhNDg3NWRkM2UzNzEyNmQwYjBiYmUxODNmNjp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfc2Vzc2lvbl9leHBpcnkiOjAsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-04 11:56:26.058141'),('twdkgmkkzmk22b0lczoocqi6v3ifdvtw','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-03-03 15:14:33.716201'),('tx868o0tt2nkbgzdt91tdkwagag2zr3v','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-11 05:29:29.082643'),('ty0q18kmv2hvvqlvifo7qycazur7rmpp','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-04-02 06:54:58.205729'),('tyb5l3o7lljsa8e96ue6gnoziz8383hp','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-02-28 13:32:43.884835'),('tyn6tjybue5fm9ovfmirk6u2zw4mbn2e','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-03-17 09:49:42.523596'),('u25jb7y9gmg3dfia1dc2udf6my3v24g7','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-06 07:22:50.735953'),('u4tufwvsynxelfg43wdaugonrvdq7ivh','ZjYzZWQ1NzMzMWZlNTNlOGM1YWRiNTAzYmEzYzlmYTRhY2UxMWNhZTp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-11 14:02:21.727958'),('u932x5hddwvucpnr5fanp7ne8e7qj27n','ZGIzYzAyMzJmOWM2MzY3NjU4NDdhMDc4M2E1NTM3NTk5ZTA4YWRiNzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-01 09:25:43.716135'),('uabktav1pjr99rj0qifeobr4k5g876fy','NGMwMmE5ZTg3ZWQxODVlZWRkYWQ0MWJhOTA4ZTgwMmVlYTI2MGZlNjp7Il9hdXRoX3VzZXJfaWQiOiI0MiIsIl9hdXRoX3VzZXJfaGFzaCI6ImZlYmVjOTM2N2QzYmNiMzEyYjgyZTllYmFkMzZlN2RjNjA2YmM0ZDUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9zZXNzaW9uX2V4cGlyeSI6MH0=','2018-05-21 09:07:05.706339'),('ub19q9i76nsbel3bnyrsglid9xn249ka','NzdlMDMyMjEwNjA2M2NhYjkxMjRhNzc1ZDA3MjI0M2NhNjM3N2EyNzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-05-17 06:09:29.856339'),('ueitm4107pn3g7wrtd7o1lmvvj90nze4','YWRiYjdmMGQxYWRlN2JhMDNlNjU3OWQ2MjA0MTZmY2QzMmY1MWQwODp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-04-19 08:24:38.631892'),('ug1t8gceqtueocp5priafl8vt4qokc36','N2Q2NWE4OGIwZDc0ZjI5MzAwZWYxZjFlNjIwNjE0ZWE2YmEwMzZiZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQyIiwiX2F1dGhfdXNlcl9oYXNoIjoiZmViZWM5MzY3ZDNiY2IzMTJiODJlOWViYWQzNmU3ZGM2MDZiYzRkNSIsIl9zZXNzaW9uX2V4cGlyeSI6MH0=','2018-05-21 11:33:58.619021'),('ui99ens2aquad36wve76latro0fjr958','YWRkNWVlNGFlOWM5MWExZjg4MjdmY2M5ODI2MGYyZGY1NWJiMTUyODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-01-15 16:45:13.328714'),('uifoa7hq6hk4g3gc1vc90pbqeqvhfabp','MGViZGIyNTdhM2EwODVmOTljZmFjOTBlY2E2M2U4YmY1ZDEyMmFmNjp7fQ==','2018-04-23 08:48:38.150457'),('ulb194848z7z12ru9yoxal0h29ms9z89','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-19 14:29:07.987588'),('umizbj4728elmuiv3c8bddtdttellnwv','YWRiYjdmMGQxYWRlN2JhMDNlNjU3OWQ2MjA0MTZmY2QzMmY1MWQwODp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-25 10:49:23.700267'),('umwhr44n6apdywo2m39det8a3ibpbtyv','ZGIzYzAyMzJmOWM2MzY3NjU4NDdhMDc4M2E1NTM3NTk5ZTA4YWRiNzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-14 14:13:19.951579'),('unxswm5mf2k05fk7pndgrhv0h4dpcxar','ZjAwODk3OWExYWQ0OTUyYTdiYTQ4N2Q5ZTgwNjYzYzVjOTExOGI3Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-05-16 09:40:04.759687'),('uoknt3cww3834wnrtm4mk8xdc2u2g954','ZWRjZTJkY2Y1M2E3Njk2ZGE5YzViODQ0MjJhMDA1YjBiOGVlYmQyNzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQ1IiwiX2F1dGhfdXNlcl9oYXNoIjoiODNiYTJjNDJlNDNiZmFmNjg0YzFmMzNhOTAxNDA0NDA5OWJmZWJhMyJ9','2018-05-08 13:48:41.271900'),('utl6n665sqnakwur795vxlfrfmsrswck','ZjYzZWQ1NzMzMWZlNTNlOGM1YWRiNTAzYmEzYzlmYTRhY2UxMWNhZTp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-13 14:38:45.566343'),('uzjbixsdzchm4uw84mzkdjp3y1ax4tg0','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-05-24 12:57:22.313903'),('vdnfth01qaumci4moqwhty3ealt4r1q6','YWRiYjdmMGQxYWRlN2JhMDNlNjU3OWQ2MjA0MTZmY2QzMmY1MWQwODp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-04-30 14:57:36.191868'),('veau397b464v0mn61xz1ool8ti5ata62','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-04-03 08:33:55.344308'),('vfvqllhvjdl46thdlr7h0plce64dvnzb','ZDcwODE3MzA5ZThlYWRhODk3MDFlZmVhMzQyNTg2ZDdlZGVmZjVmMTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-12-29 14:38:03.268214'),('vgxv9cvjjqqermrwn1r428611p1kdvgj','YWRkNWVlNGFlOWM5MWExZjg4MjdmY2M5ODI2MGYyZGY1NWJiMTUyODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-01-14 17:33:05.920443'),('vjr590g2j2d6dov0trxcr2d8k3hkm3os','ZGIzYzAyMzJmOWM2MzY3NjU4NDdhMDc4M2E1NTM3NTk5ZTA4YWRiNzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-05-17 14:07:04.414081'),('vk70bixyfx8ytnc73zt9fy1oj01vmgj7','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-04-23 14:07:32.721241'),('vkn3x346bh4yn9zci7shi3hl3siyx4ad','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-19 15:51:51.029253'),('vv5nqptfdvbenwm8hx4q8wyj8ex67l9a','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-04-01 06:20:49.781161'),('vx9i88wpvv0mbt3393wof1i6dgvp0ad2','OTgyYmY2YzlkY2ViY2NlMTY2YWM4OGYwODc4OWRiMzc1Y2E5NTUyYzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-07 15:49:58.607590'),('w18m25ggvvc5tq19k06qcsgwwjn3ykir','ZDcwODE3MzA5ZThlYWRhODk3MDFlZmVhMzQyNTg2ZDdlZGVmZjVmMTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-12-29 08:21:56.572347'),('w20awghlj1yizjjpwljqzgyk5ahaa60y','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-04-10 16:00:25.413699'),('w3z0rengs78m503kcogyrcx8d0oe60il','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-15 11:25:21.812889'),('w87hj75c81wdgs6hhhmbl9gbhs975tm4','MTc2ZGE4NjNkM2E0N2FiNDMxYTVmYTJjZjFkYjRmNWFhM2NlMTZjNTp7Il9hdXRoX3VzZXJfaGFzaCI6ImZlYmVjOTM2N2QzYmNiMzEyYjgyZTllYmFkMzZlN2RjNjA2YmM0ZDUiLCJfYXV0aF91c2VyX2lkIjoiNDIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9','2018-05-24 10:26:06.342392'),('wadw48onhsnqqibv099iahm6ol2bslnb','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-05 12:51:53.383485'),('wdg6tpzsz85mlxx95tyd0j92bfle1f1e','NzhhMDA2YTJiZjM0MjE1NzAyZmJjOWVmNGM2MTUwYTk1ODNkMzBmYjp7Il9hdXRoX3VzZXJfaWQiOiIyOCIsIl9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJmOTY5MTgwOTNlNjJjMWJhZWRiNjFhMTE4ODNjZTg2ZTU5OGRhYmMwIn0=','2018-05-20 12:12:04.426392'),('wdqcxc5rab65q9nfd4txwysd7wgfzlqp','MDFkMGNmZTRiYTU2NGM2NmJlMWE5MTZhMGY5YjBmZGVkN2EzYjMwMDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-04-08 08:56:41.869136'),('wevhu4wpa423cmrlt82h0hkjsm9otq3f','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-22 14:04:54.406783'),('wjrwyjtv2yup2audjhps0z2db259fvcc','NjE4NjNiNzNkNWFiY2NhYmU1NDVkM2NiZDNmYzliYjNlY2ViMzM5Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-01-21 15:44:11.895841'),('wmiino1yc35jgjbbkxgbgodhbuwnwsjf','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-05-23 17:33:15.045157'),('wq09rfuu3ngw0u8q4nznz55tkne7afox','ZjYzZWQ1NzMzMWZlNTNlOGM1YWRiNTAzYmEzYzlmYTRhY2UxMWNhZTp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-03-24 11:47:22.713534'),('wrcwlouhn7dcfvmeltjo18ayvp96k5co','ZjYzZWQ1NzMzMWZlNTNlOGM1YWRiNTAzYmEzYzlmYTRhY2UxMWNhZTp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-13 18:36:54.939674'),('wtf0oe4kz7a18hzvp20qcvyjb90kbmax','YWRiYjdmMGQxYWRlN2JhMDNlNjU3OWQ2MjA0MTZmY2QzMmY1MWQwODp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-14 05:59:29.540884'),('wunwo5uaxvpqi40gcky7xiiaitur4q13','OTgyYmY2YzlkY2ViY2NlMTY2YWM4OGYwODc4OWRiMzc1Y2E5NTUyYzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-08 16:29:19.120890'),('wx0dv0sgs72od86mz73jhvnz2yd6x8rc','ZDcwODE3MzA5ZThlYWRhODk3MDFlZmVhMzQyNTg2ZDdlZGVmZjVmMTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-12-28 18:07:15.370597'),('wzs62v9krjp0azivux03zpgf13cxg55r','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-02-28 15:05:36.350271'),('x44vpf27yc7blqd0j6gre7uyklrc1zuj','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-11 06:41:01.641430'),('x5x7oqh9n6132axytkftzg11pjp0pkif','YjZjM2IyZjlkOTI4NTJlODZiMTRmNjljYzQ4YzYyMGEwYWVhOTg2Zjp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-02 03:40:46.310234'),('xcf9e9qfkua7bi5oqgbnjnz0xwcfdgc4','OTgyYmY2YzlkY2ViY2NlMTY2YWM4OGYwODc4OWRiMzc1Y2E5NTUyYzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-12-31 03:05:47.923966'),('xkg3mq4hatx4c2jq99h51hcsf0jb1pb0','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-03 12:27:22.122446'),('xkhdbudnjnur927wwpzgc9gm8tbwar45','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-03-03 15:12:31.177145'),('xqefz9bs8eya6sch66142sk1myuu63zz','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-04-30 13:54:41.020051'),('xqf2uuzzyonc8sd6xcqg6ujdjsyaneyj','MTc2ZGE4NjNkM2E0N2FiNDMxYTVmYTJjZjFkYjRmNWFhM2NlMTZjNTp7Il9hdXRoX3VzZXJfaGFzaCI6ImZlYmVjOTM2N2QzYmNiMzEyYjgyZTllYmFkMzZlN2RjNjA2YmM0ZDUiLCJfYXV0aF91c2VyX2lkIjoiNDIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9','2018-05-07 13:52:35.718273'),('xuyos8289pxk57t2rvg80bnmycsehxj1','YTg1OWE4ZDRiOGM4OTdlYjdhYzI4YTAxYmJkOTY5ZDU0NmZmNjhlNDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-03-29 13:34:23.301163'),('xykxhl8piawz7mk3m78zbjikttbpr1vv','YjM4NmZjMWE3YTVlZTYxM2U0MDUxZTg1ZjllOWE1M2NiY2E2N2RhMTp7Il9hdXRoX3VzZXJfaGFzaCI6ImZlYmVjOTM2N2QzYmNiMzEyYjgyZTllYmFkMzZlN2RjNjA2YmM0ZDUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI0MiJ9','2018-05-08 07:33:36.016229'),('y0nhl58edh0ejs6z3rrz9uw94u37m1nn','ZGIzYzAyMzJmOWM2MzY3NjU4NDdhMDc4M2E1NTM3NTk5ZTA4YWRiNzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX2F1dGhfdXNlcl9pZCI6IjEifQ==','2018-01-13 22:23:34.392268'),('y13huwqkp33ugduk9w590ugjbwfreyp2','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-24 13:44:51.114422'),('y3td4lv14c3ptf84y2gibg9yh822cq3l','YWRkNWVlNGFlOWM5MWExZjg4MjdmY2M5ODI2MGYyZGY1NWJiMTUyODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-01-20 19:48:09.372607'),('y4msza738ydjlpzblunfbosu6om67w44','N2YxNzcxZGI3MzM0NWExZDY2OTM2ZTU5NTVlM2Q0NzM3NzgyZTNjODp7Il9hdXRoX3VzZXJfaGFzaCI6IjhiMWJjNGJjYjUzN2NkMWU3OWQ2N2Y4MTk5NzQ1Y2RhOTQwMTAzZGEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9pZCI6IjQ4In0=','2018-05-16 13:12:00.514721'),('y8geu46beanfyevz8t3y5ld48fd2ob5t','NTk1YTcyZDhmNzVkZWFjMWJkYzQ0N2I1MTcwNjhiNTYyYTA3ZmQ0ZTp7Il9hdXRoX3VzZXJfaGFzaCI6IjQ1OTBlMjQzOWJhZDU0NzAwODRiZTVjZjE5ZmFhNzQ4ZWZlYmFkNDkiLCJfYXV0aF91c2VyX2lkIjoiNTAiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9zZXNzaW9uX2V4cGlyeSI6MH0=','2018-05-20 07:15:37.536536'),('ycyptbtj1ndeusatnvhjuf3sguki1dvl','YWZlZTRkYWI0YmNiMjkxOWM4YjkzMTQyZTk2MDBmYmZlNWJkOGRkMzp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-05-02 14:07:04.815016'),('yhbk76bzn05kmp5r7adxchrammyne3u5','OGJjYWNjZTI4M2ZjMDc4NGFhYWE3YjUzNDY3NTQ0M2QxYmNhZDBhOTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZjBjNjI1YjAxYTg1ZDc3YjU5ZDU4NjYxNjMxNWE1NzdmOWJiODA0MSIsIl9hdXRoX3VzZXJfaWQiOiI0OCJ9','2018-05-24 15:59:19.020583'),('ylmlembyqme9gct1usgwi48y9iod4aan','YTkwZDhiOWZiM2I0MmVkNjcxMGJlODdiY2QwZTAzZjcxMTE0YjA0MTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2017-12-30 07:47:29.402043'),('ypnzegy7tzln142ht9je0n24jejhkwoy','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-02-24 09:37:11.368250'),('ypzfww4jrve9j509rkszk8t8ge8lgfos','YWRiYjdmMGQxYWRlN2JhMDNlNjU3OWQ2MjA0MTZmY2QzMmY1MWQwODp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-05-01 14:30:55.434917'),('ytuwcknvp0vk9m9rpce44ubqjohljby3','NWVlYjBiODUzNTg5NTNjOGJiY2E3ZTI5NmQ5NWI1ZWZiZGM4NzNhNzp7Il9hdXRoX3VzZXJfaWQiOiI0OCIsIl9hdXRoX3VzZXJfaGFzaCI6IjhiMWJjNGJjYjUzN2NkMWU3OWQ2N2Y4MTk5NzQ1Y2RhOTQwMTAzZGEiLCJfc2Vzc2lvbl9leHBpcnkiOjAsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-05-17 08:41:09.626568'),('yzl8r05u9jlge2royvmrr1q2ov88l05y','YWRiYjdmMGQxYWRlN2JhMDNlNjU3OWQ2MjA0MTZmY2QzMmY1MWQwODp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-03 07:53:43.776982'),('z063em0gq851u8er2mdlv9kpgchqr5p8','OTgyYmY2YzlkY2ViY2NlMTY2YWM4OGYwODc4OWRiMzc1Y2E5NTUyYzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-12-31 05:36:25.773412'),('z1irdqi3dmalq3wfgbuogmf5qppplrlp','YWRkNWVlNGFlOWM5MWExZjg4MjdmY2M5ODI2MGYyZGY1NWJiMTUyODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-01-20 13:47:11.612459'),('z20ph100uabx0utn5yavxkpk82fszala','ZWI3ZDRhODJiNWFlZTFmMTFkNTQzZTM3M2FiMzQzYjhmNDlhMWM2ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-04-24 13:28:38.051635'),('z89dljkck2vbbarryj2v02q740dty1d7','ZjAwODk3OWExYWQ0OTUyYTdiYTQ4N2Q5ZTgwNjYzYzVjOTExOGI3Mjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2017-12-31 13:12:05.482727'),('za58otuq6ar3zy8s3d83u4ozeeb19wc3','NTAyNzY0YmFlNDIyN2ZjZDZmZmU2ZDBiZWQ4ZTE2NDY2M2U4OGVmNTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-03-21 08:08:32.023384'),('zc0e6xnoaib8glqyiilviksf5drmdskk','Y2FkZDlkZDIwZGM5YzlhNzZlMTIxMDE2NmZmNjMxYTg5ZThiMmQ0ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjI4IiwiX2F1dGhfdXNlcl9oYXNoIjoiZjk2OTE4MDkzZTYyYzFiYWVkYjYxYTExODgzY2U4NmU1OThkYWJjMCJ9','2018-05-01 19:09:17.240154'),('zdp4zmp8puc2z83tftfazzovtku1rk93','Njc0MzQyOTU0NjJiNjcxYzFiOWFhMjlkMjVlZTBiZmYwOGJhMTg1MDp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-01-22 11:53:00.058546'),('zh3lrorsbypcv13cz9rmbont84fbviyl','Y2FkZDlkZDIwZGM5YzlhNzZlMTIxMDE2NmZmNjMxYTg5ZThiMmQ0ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjI4IiwiX2F1dGhfdXNlcl9oYXNoIjoiZjk2OTE4MDkzZTYyYzFiYWVkYjYxYTExODgzY2U4NmU1OThkYWJjMCJ9','2018-05-03 12:35:03.427069'),('zh3wmaobbjg7fa1bcf4dugl9iua84bg5','YWRkNWVlNGFlOWM5MWExZjg4MjdmY2M5ODI2MGYyZGY1NWJiMTUyODp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX3Nlc3Npb25fZXhwaXJ5IjowfQ==','2018-01-14 16:33:39.018892'),('zisxisz180djnuodtrco6pmvfawab0fu','NzE2MWE2NmY2OWY1MmU2OTg2YTYzYWNkMzQyYTgyMTc0NmUxN2Q0ZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2018-03-13 14:53:28.195819'),('zm2u6jtktr7q8i22et8q7vd5wmwv2osj','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-04-23 11:20:29.501707'),('zn45qlglhraxi6hyxx8lyhdpvjf86h84','ZDVlMmQwY2JjMmI3MDVhNDg3NWRkM2UzNzEyNmQwYjBiYmUxODNmNjp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfc2Vzc2lvbl9leHBpcnkiOjAsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-06 08:34:25.589799'),('zp2b7f2jmkou535h78icxb215pr2dmpv','YzRlYjU3NzhiNTE4ODlkMDEzMDgxNjY3ZjBjNmJlODVkNjk4OWFlYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5ZGQ0MDlhYmZlOGFmNjI5YmU4ZTBjMTdkODVmZGYxMDg2N2Q5NzRmIn0=','2018-05-23 06:15:27.983310'),('zq839qt3oyljmqeirgbs84rdbzpppw6b','N2MzODVkNDk5NmQyNDE4M2I4NGMwNjFiZmMwNjU1YTJkMGU4MDNjYTp7Il9hdXRoX3VzZXJfaGFzaCI6IjlkZDQwOWFiZmU4YWY2MjliZThlMGMxN2Q4NWZkZjEwODY3ZDk3NGYiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-05-22 08:01:11.608226'),('zxhdfloscr776punlvd3mgqez3dj9laf','OTgyYmY2YzlkY2ViY2NlMTY2YWM4OGYwODc4OWRiMzc1Y2E5NTUyYzp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiOWRkNDA5YWJmZThhZjYyOWJlOGUwYzE3ZDg1ZmRmMTA4NjdkOTc0ZiIsIl9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-03-03 08:51:20.519183'),('zz9itozj6ibw7mkggdn18wesdlr1e97l','NzE3NjViNDcyMWFhNGNiNzA0NjRjODU1ZmNhM2I1N2EzMDRjNTU2OTp7Il9zZXNzaW9uX2V4cGlyeSI6MCwiX2F1dGhfdXNlcl9oYXNoIjoiNGIwMjY5YTAxMGUzOTI2YmY5MjUxZWJhOTkzYjIxNGVjOWQyZmRiMCIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQifQ==','2018-01-08 00:43:22.501019');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_site`
--

DROP TABLE IF EXISTS `django_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_site` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_site_domain_a2e37b91_uniq` (`domain`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_site`
--

LOCK TABLES `django_site` WRITE;
/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;
INSERT INTO `django_site` VALUES (1,'shuni.live','shuni.live');
/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `otp_hotp_hotpdevice`
--

DROP TABLE IF EXISTS `otp_hotp_hotpdevice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `otp_hotp_hotpdevice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `confirmed` tinyint(1) NOT NULL,
  `key` varchar(80) NOT NULL,
  `digits` smallint(5) unsigned NOT NULL,
  `tolerance` smallint(5) unsigned NOT NULL,
  `counter` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `otp_hotp_hotpdevice_user_id_831afee3_fk_auth_user_id` (`user_id`),
  CONSTRAINT `otp_hotp_hotpdevice_user_id_831afee3_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `otp_hotp_hotpdevice`
--

LOCK TABLES `otp_hotp_hotpdevice` WRITE;
/*!40000 ALTER TABLE `otp_hotp_hotpdevice` DISABLE KEYS */;
/*!40000 ALTER TABLE `otp_hotp_hotpdevice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `otp_static_staticdevice`
--

DROP TABLE IF EXISTS `otp_static_staticdevice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `otp_static_staticdevice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `confirmed` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `otp_static_staticdevice_user_id_7f9cff2b_fk_auth_user_id` (`user_id`),
  CONSTRAINT `otp_static_staticdevice_user_id_7f9cff2b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `otp_static_staticdevice`
--

LOCK TABLES `otp_static_staticdevice` WRITE;
/*!40000 ALTER TABLE `otp_static_staticdevice` DISABLE KEYS */;
/*!40000 ALTER TABLE `otp_static_staticdevice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `otp_static_statictoken`
--

DROP TABLE IF EXISTS `otp_static_statictoken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `otp_static_statictoken` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(16) NOT NULL,
  `device_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `otp_static_statictok_device_id_74b7c7d1_fk_otp_stati` (`device_id`),
  KEY `otp_static_statictoken_token_d0a51866` (`token`),
  CONSTRAINT `otp_static_statictok_device_id_74b7c7d1_fk_otp_stati` FOREIGN KEY (`device_id`) REFERENCES `otp_static_staticdevice` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `otp_static_statictoken`
--

LOCK TABLES `otp_static_statictoken` WRITE;
/*!40000 ALTER TABLE `otp_static_statictoken` DISABLE KEYS */;
/*!40000 ALTER TABLE `otp_static_statictoken` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `otp_totp_totpdevice`
--

DROP TABLE IF EXISTS `otp_totp_totpdevice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `otp_totp_totpdevice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `confirmed` tinyint(1) NOT NULL,
  `key` varchar(80) NOT NULL,
  `step` smallint(5) unsigned NOT NULL,
  `t0` bigint(20) NOT NULL,
  `digits` smallint(5) unsigned NOT NULL,
  `tolerance` smallint(5) unsigned NOT NULL,
  `drift` smallint(6) NOT NULL,
  `last_t` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `otp_totp_totpdevice_user_id_0fb18292_fk_auth_user_id` (`user_id`),
  CONSTRAINT `otp_totp_totpdevice_user_id_0fb18292_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `otp_totp_totpdevice`
--

LOCK TABLES `otp_totp_totpdevice` WRITE;
/*!40000 ALTER TABLE `otp_totp_totpdevice` DISABLE KEYS */;
/*!40000 ALTER TABLE `otp_totp_totpdevice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `otp_yubikey_remoteyubikeydevice`
--

DROP TABLE IF EXISTS `otp_yubikey_remoteyubikeydevice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `otp_yubikey_remoteyubikeydevice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `confirmed` tinyint(1) NOT NULL,
  `public_id` varchar(32) NOT NULL,
  `service_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `otp_yubikey_remoteyu_service_id_63162285_fk_otp_yubik` (`service_id`),
  KEY `otp_yubikey_remoteyubikeydevice_user_id_80de3c1b_fk_auth_user_id` (`user_id`),
  CONSTRAINT `otp_yubikey_remoteyu_service_id_63162285_fk_otp_yubik` FOREIGN KEY (`service_id`) REFERENCES `otp_yubikey_validationservice` (`id`),
  CONSTRAINT `otp_yubikey_remoteyubikeydevice_user_id_80de3c1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `otp_yubikey_remoteyubikeydevice`
--

LOCK TABLES `otp_yubikey_remoteyubikeydevice` WRITE;
/*!40000 ALTER TABLE `otp_yubikey_remoteyubikeydevice` DISABLE KEYS */;
/*!40000 ALTER TABLE `otp_yubikey_remoteyubikeydevice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `otp_yubikey_validationservice`
--

DROP TABLE IF EXISTS `otp_yubikey_validationservice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `otp_yubikey_validationservice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `api_id` int(11) NOT NULL,
  `api_key` varchar(64) NOT NULL,
  `base_url` varchar(200) NOT NULL,
  `api_version` varchar(8) NOT NULL,
  `use_ssl` tinyint(1) NOT NULL,
  `param_sl` varchar(16) NOT NULL,
  `param_timeout` varchar(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `otp_yubikey_validationservice`
--

LOCK TABLES `otp_yubikey_validationservice` WRITE;
/*!40000 ALTER TABLE `otp_yubikey_validationservice` DISABLE KEYS */;
/*!40000 ALTER TABLE `otp_yubikey_validationservice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `otp_yubikey_yubikeydevice`
--

DROP TABLE IF EXISTS `otp_yubikey_yubikeydevice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `otp_yubikey_yubikeydevice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `confirmed` tinyint(1) NOT NULL,
  `private_id` varchar(12) NOT NULL,
  `key` varchar(32) NOT NULL,
  `session` int(10) unsigned NOT NULL,
  `counter` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `otp_yubikey_yubikeydevice_user_id_80a424a7_fk_auth_user_id` (`user_id`),
  CONSTRAINT `otp_yubikey_yubikeydevice_user_id_80a424a7_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `otp_yubikey_yubikeydevice`
--

LOCK TABLES `otp_yubikey_yubikeydevice` WRITE;
/*!40000 ALTER TABLE `otp_yubikey_yubikeydevice` DISABLE KEYS */;
/*!40000 ALTER TABLE `otp_yubikey_yubikeydevice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `socialaccount_socialaccount`
--

DROP TABLE IF EXISTS `socialaccount_socialaccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `socialaccount_socialaccount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider` varchar(30) NOT NULL,
  `uid` varchar(191) NOT NULL,
  `last_login` datetime(6) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  `extra_data` longtext NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `socialaccount_socialaccount_provider_uid_fc810c6e_uniq` (`provider`,`uid`),
  KEY `socialaccount_socialaccount_user_id_8146e70c_fk_auth_user_id` (`user_id`),
  CONSTRAINT `socialaccount_socialaccount_user_id_8146e70c_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `socialaccount_socialaccount`
--

LOCK TABLES `socialaccount_socialaccount` WRITE;
/*!40000 ALTER TABLE `socialaccount_socialaccount` DISABLE KEYS */;
/*!40000 ALTER TABLE `socialaccount_socialaccount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `socialaccount_socialapp`
--

DROP TABLE IF EXISTS `socialaccount_socialapp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `socialaccount_socialapp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `provider` varchar(30) NOT NULL,
  `name` varchar(40) NOT NULL,
  `client_id` varchar(191) NOT NULL,
  `secret` varchar(191) NOT NULL,
  `key` varchar(191) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `socialaccount_socialapp`
--

LOCK TABLES `socialaccount_socialapp` WRITE;
/*!40000 ALTER TABLE `socialaccount_socialapp` DISABLE KEYS */;
/*!40000 ALTER TABLE `socialaccount_socialapp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `socialaccount_socialapp_sites`
--

DROP TABLE IF EXISTS `socialaccount_socialapp_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `socialaccount_socialapp_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `socialapp_id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `socialaccount_socialapp_sites_socialapp_id_site_id_71a9a768_uniq` (`socialapp_id`,`site_id`),
  KEY `socialaccount_socialapp_sites_site_id_2579dee5_fk_django_site_id` (`site_id`),
  CONSTRAINT `socialaccount_social_socialapp_id_97fb6e7d_fk_socialacc` FOREIGN KEY (`socialapp_id`) REFERENCES `socialaccount_socialapp` (`id`),
  CONSTRAINT `socialaccount_socialapp_sites_site_id_2579dee5_fk_django_site_id` FOREIGN KEY (`site_id`) REFERENCES `django_site` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `socialaccount_socialapp_sites`
--

LOCK TABLES `socialaccount_socialapp_sites` WRITE;
/*!40000 ALTER TABLE `socialaccount_socialapp_sites` DISABLE KEYS */;
/*!40000 ALTER TABLE `socialaccount_socialapp_sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `socialaccount_socialtoken`
--

DROP TABLE IF EXISTS `socialaccount_socialtoken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `socialaccount_socialtoken` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` longtext NOT NULL,
  `token_secret` longtext NOT NULL,
  `expires_at` datetime(6) DEFAULT NULL,
  `account_id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `socialaccount_socialtoken_app_id_account_id_fca4e0ac_uniq` (`app_id`,`account_id`),
  KEY `socialaccount_social_account_id_951f210e_fk_socialacc` (`account_id`),
  CONSTRAINT `socialaccount_social_account_id_951f210e_fk_socialacc` FOREIGN KEY (`account_id`) REFERENCES `socialaccount_socialaccount` (`id`),
  CONSTRAINT `socialaccount_social_app_id_636a42d7_fk_socialacc` FOREIGN KEY (`app_id`) REFERENCES `socialaccount_socialapp` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `socialaccount_socialtoken`
--

LOCK TABLES `socialaccount_socialtoken` WRITE;
/*!40000 ALTER TABLE `socialaccount_socialtoken` DISABLE KEYS */;
/*!40000 ALTER TABLE `socialaccount_socialtoken` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `two_factor_phonedevice`
--

DROP TABLE IF EXISTS `two_factor_phonedevice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `two_factor_phonedevice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `confirmed` tinyint(1) NOT NULL,
  `number` varchar(128) NOT NULL,
  `key` varchar(40) NOT NULL,
  `method` varchar(4) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `two_factor_phonedevice_user_id_54718003_fk_auth_user_id` (`user_id`),
  CONSTRAINT `two_factor_phonedevice_user_id_54718003_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `two_factor_phonedevice`
--

LOCK TABLES `two_factor_phonedevice` WRITE;
/*!40000 ALTER TABLE `two_factor_phonedevice` DISABLE KEYS */;
/*!40000 ALTER TABLE `two_factor_phonedevice` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-11  4:42:30