#!/usr/bin/env bash
#Step 1 - Setup Management Node

#A. Download the MySQL Cluster software
cd ~
wget http://dev.mysql.com/get/Downloads/MySQL-Cluster-7.4/MySQL-Cluster-gpl-7.4.10-1.el7.x86_64.rpm-bundle.tar
tar -xvf MySQL-Cluster-gpl-7.4.10-1.el7.x86_64.rpm-bundle.tar
ls -lah *.rpm
#B. Install and Remove Packages
yum -y install perl-Data-Dumper
#yum -y remove mariadb-libs
#D. Configure MySQL Cluster
mkdir -p /var/lib/mysql-cluster
cp ./config.ini /var/lib/mysql-cluster/


#C. Install MySQL Cluster
cd ~
rpm -Uvh MySQL-Cluster-client-gpl-7.4.10-1.el7.x86_64.rpm
rpm -Uvh MySQL-Cluster-server-gpl-7.4.10-1.el7.x86_64.rpm
rpm -Uvh MySQL-Cluster-shared-gpl-7.4.10-1.el7.x86_64.rpm


#E. Start the Management Node
ndb_mgmd --config-file=/var/lib/mysql-cluster/config.ini



#TEST
#ndb_mgm
#show