#!/usr/bin/env bash
#docker network create cluster_net --subnet=192.168.0.0/16
#MANAGEMENT (MGM)
#docker run -d --net=cluster_net --name=management1 --ip=192.168.0.2 -v /home/uzzal/PycharmProjects/shuni_docker/ndb_cluster/ndb_cluster/mysql-cluster.cnf:/etc/mysql-cluster.cnf mysql/mysql-cluster ndb_mgmd
docker run -d --net=cluster_net --name=management --ip=192.168.0.2  uzzal2k5/mysql-cluster:latest ndb_mgmd

#DATA NODE (NDB)
docker run -d --net=cluster_net --name=ndb1 --ip=192.168.0.3  uzzal2k5/mysql-cluster:latest ndbd
docker run -d --net=cluster_net --name=ndb2 --ip=192.168.0.4  uzzal2k5/mysql-cluster:latest ndbd
#docker run -d --net=cluster_net --name=ndb3 --ip=192.168.0.5  uzzal2k5/mysql-cluster:latest ndbd

#MYSQL NODE (API)
docker run -d --net=cluster_net --name=mysql_api10 --ip=192.168.0.10  uzzal2k5/mysql-cluster:latest mysqld
docker run -d --net=cluster_net --name=mysql_api11 --ip=192.168.0.11  uzzal2k5/mysql-cluster:latest mysqld
docker run -d --net=cluster_net --name=mysql_api12 --ip=192.168.0.12  uzzal2k5/mysql-cluster:latest mysqld
